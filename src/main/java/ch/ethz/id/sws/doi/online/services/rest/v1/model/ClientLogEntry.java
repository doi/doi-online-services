package ch.ethz.id.sws.doi.online.services.rest.v1.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ClientLogEntry {
    private String severity;
    private Long timestamp;
    private String message;
    private Long column;
    private Long line;
    private String stacktrace;
    private String browser;
    private String client;
    private String version;
    
    public String getSeverity() {
        return severity;
    }
    
    public void setSeverity(String severity) {
        this.severity = severity;
    }
    
    public Long getTimestamp() {
        return timestamp;
    }
    
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public Long getColumn() {
        return column;
    }
    
    public void setColumn(Long column) {
        this.column = column;
    }
    
    public Long getLine() {
        return line;
    }
    
    public void setLine(Long line) {
        this.line = line;
    }
    
    public String getStacktrace() {
        return stacktrace;
    }
    
    public void setStacktrace(String stacktrace) {
        this.stacktrace = stacktrace;
    }
    
    public String getBrowser() {
        return browser;
    }
    
    public void setBrowser(String browser) {
        this.browser = browser;
    }
    
    public String getClient() {
        return client;
    }
    
    public void setClient(String client) {
        this.client = client;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
