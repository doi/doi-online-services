package ch.ethz.id.sws.doi.online.services.rest.v1.mapper;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;

import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;

import ch.ethz.id.sws.base.commons.context.B3ApplicationContext;
import ch.ethz.id.sws.base.commons.version.VersionManager;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.User;
import ch.ethz.id.sws.security.commons.ShibbolethAuthToken;
import ch.ethz.id.sws.security.commons.ShibbolethUserDetails;

@Mapper(componentModel = "spring")
public abstract class UserMapper {

    @Autowired
    private B3ApplicationContext b3ApplicationContext = null;

    @Autowired
    private VersionManager versionManager = null;

    @Mappings({
            @Mapping(target = "originIP", source = "originIp"),
            @Mapping(target = "signOnAppId", source = "applicationId"),
            @Mapping(target = "affiliations", source = "affiliations"),
            @Mapping(target = "homeOrgType", source = "homeOrgType"),
            @Mapping(target = "authDate", source = "authDate", dateFormat = "yyyy-MM-dd HH:mm:ss"),
            @Mapping(target = "birthdate", source = "birthdate", dateFormat = "yyyy-MM-dd"),
            @Mapping(target = "grantedAuthorities", ignore = true),
            @Mapping(target = "persId", source = "persId"),
            @Mapping(target = "email", source = "email"),
            @Mapping(target = "language", source = "language"),
            @Mapping(target = "loginname", source = "username"),
            @Mapping(target = "geschlecht", source = "gender"),
            @Mapping(target = "anrede", source = "title"),
            @Mapping(target = "nPid", source = "NPid"),
    })
    public abstract User map(ShibbolethUserDetails shibbolethUserDetails,
            @Context ShibbolethAuthToken shibbolethAuthToken);

    @AfterMapping
    protected void afterUserMapping(final ShibbolethUserDetails shibbolethUserDetails,
            @Context final ShibbolethAuthToken shibbolethAuthToken, @MappingTarget final User user) {
        user.getServerVersion().setServerName(shibbolethUserDetails.getServerName());
        user.getServerVersion().setDevStage(this.b3ApplicationContext.getDevelopmentLevel());
        user.getServerVersion().setSystemStartupTime(ManagementFactory.getRuntimeMXBean().getStartTime());
        user.getServerVersion().setImplementationBranch(this.versionManager.getMainWarInfo().getBranch());
        user.getServerVersion().setImplementationBuild(this.versionManager.getMainWarInfo().getBuildNmbr());
        user.getServerVersion().setImplementationBuildDate(this.versionManager.getMainWarInfo().getBuildDate());
        user.getServerVersion().setImplementationVersion(this.versionManager.getMainWarInfo().getVersion());

        user.setGrantedAuthorities(new ArrayList<GrantedAuthority>(shibbolethAuthToken.getAuthorities()));

    }
}
