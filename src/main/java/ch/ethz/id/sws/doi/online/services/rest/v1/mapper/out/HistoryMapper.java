package ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out;

import java.util.List;

import org.mapstruct.Mapper;

import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.commons.services.doihistory.DomObjDOIHistory;
import ch.ethz.id.sws.doi.commons.services.doihistory.DomObjDOIHistoryResultat;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.DOIOUT;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.History;

@Mapper(componentModel = "spring")
public abstract class HistoryMapper {

    public abstract History map(DomObjDOIHistoryResultat domObjDOIHistoryResultat);

    public abstract List<DOIOUT> map(List<DomObjDOIHistory> domObjDOIHistoryList);

    public abstract DOIOUT map(DomObjDOI domObjDOI);
}
