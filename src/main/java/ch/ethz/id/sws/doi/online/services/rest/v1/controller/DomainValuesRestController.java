package ch.ethz.id.sws.doi.online.services.rest.v1.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.ethz.id.sws.base.commons.Sprache;
import ch.ethz.id.sws.base.services.rest.v1.model.out.RestError;
import ch.ethz.id.sws.doi.commons.services.domains.DOIDomainDAO;
import ch.ethz.id.sws.doi.commons.services.domains.DomainCacheLoaderDOIDomain;
import ch.ethz.id.sws.doi.online.services.rest.RestCommons;
import ch.ethz.id.sws.doi.online.services.rest.RestErrorCodes;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out.DomainMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out.DomainValueMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.Domain;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheEntry;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheService;

@RestController
@RequestMapping(value = "/v1/domains")
public class DomainValuesRestController {
    private static Log LOG = LogFactory.getLog(DomainValuesRestController.class);

    public static final String PARAM_INCL_INACTIVE = "inactive-incl";
    public static final String PARAM_LANGUAGE = "lang";
    public static final String PARAM_SEARCHTERM = "searchTerm";
    public static final String PARAM_SEARCHSHORTTXT = "searchShortText";

    @Autowired
    private DomainCacheService domainCacheService = null;

    @Autowired
    private DomainValueMapper domainValueMapper = null;

    @Autowired
    private DomainMapper domainMapper = null;

    @Autowired
    private DOIDomainDAO doiDomainDAO = null;

    @PostConstruct
    protected void initCache() {
        this.domainCacheService.addCustomDomain(DOIDomainDAO.DOM_RESTYPE_GENERAL,
                new DomainCacheLoaderDOIDomain(this.doiDomainDAO));
        this.domainCacheService.addCustomDomain(DOIDomainDAO.DOM_BATCH_STATUS,
                new DomainCacheLoaderDOIDomain(this.doiDomainDAO));
        this.domainCacheService.addCustomDomain(DOIDomainDAO.DOM_IMPORT_TYPE,
                new DomainCacheLoaderDOIDomain(this.doiDomainDAO));
    }

    @ResponseBody
    @CrossOrigin
    @GetMapping(value = "/{domName}", produces = MediaType.APPLICATION_JSON_VALUE, params = {
            PARAM_LANGUAGE })
    public Object getDomainData(HttpServletRequest request,
            @PathVariable String domName,
            @RequestParam(value = PARAM_INCL_INACTIVE, required = false, defaultValue = "false") Boolean inclInactive,
            @RequestParam(value = PARAM_LANGUAGE, required = true) String language)
            throws Exception {

        LOG.debug("REST getDomainData(domName = " + domName + ", lang = " + language);

        Sprache sprache = RestCommons.validateLanguage(language);
        Domain domain = this.domainMapper.map(domName, inclInactive, sprache);

        if (domain.getDomainValueArray() == null) {
            RestError.throwException(LOG, "Domain", new Object[] { domName }, RestErrorCodes.GLOBAL_ELEMENT_NOTFOUND);
        }

        return domain;
    }

    @ResponseBody
    @CrossOrigin
    @GetMapping(value = "/{domName}", produces = MediaType.APPLICATION_JSON_VALUE, params = {
            PARAM_LANGUAGE, PARAM_SEARCHTERM })
    public Object searchDomain(HttpServletRequest request,
            @PathVariable String domName,
            @RequestParam(value = PARAM_SEARCHSHORTTXT, required = false) Boolean searchShortText,
            @RequestParam(value = PARAM_SEARCHTERM, required = true) String searchTerm,
            @RequestParam(value = PARAM_LANGUAGE, required = true) String language,
            @RequestParam(value = PARAM_INCL_INACTIVE, required = false, defaultValue = "false") Boolean inclInactive)
            throws Exception {

        LOG.debug("REST searchDomain(domName = " + domName + ", lang = " + language + ", searchTerm = " + searchTerm);

        Sprache sprache = RestCommons.validateLanguage(language);
        List<DomainCacheEntry> domList = this.domainCacheService.searchDomainEntries(
                domName, searchTerm, searchShortText, !inclInactive, Sprache.fromISOCode(language));
        return this.domainValueMapper.mapList(domList, sprache);
    }

    @ResponseBody
    @CrossOrigin
    @GetMapping(value = "/{domName}/values/{value}", produces = MediaType.APPLICATION_JSON_VALUE, params = {
            PARAM_LANGUAGE })
    public Object getTextForValue(HttpServletRequest request,
            @PathVariable String domName,
            @PathVariable Integer value,
            @RequestParam(value = PARAM_LANGUAGE, required = true) String language) throws Exception {

        LOG.debug("REST getTextForValue(domName = " + domName + ", lang = " + language + ", value = " + value);

        Sprache sprache = RestCommons.validateLanguage(language);
        DomainCacheEntry domainCacheEntry = this.domainCacheService.getDomainEntryForKey(domName, value);
        return this.domainValueMapper.map(domainCacheEntry, sprache);
    }

    @ResponseBody
    @CrossOrigin
    @GetMapping(value = "/{domName}/texts/{text}", produces = MediaType.APPLICATION_JSON_VALUE, params = {
            PARAM_LANGUAGE })
    public Object getValueForText(HttpServletRequest request,
            @PathVariable String domName,
            @PathVariable String text,
            @RequestParam(value = PARAM_SEARCHSHORTTXT, required = false) Boolean isShortText,
            @RequestParam(value = PARAM_LANGUAGE, required = true) String language) throws Exception {

        LOG.debug("REST getValueForText(domName = " + domName + ", lang = " + language + ", text = " + text);

        Sprache sprache = RestCommons.validateLanguage(language);
        DomainCacheEntry domainCacheEntry = this.domainCacheService.getDomainEntryForText(domName, text, isShortText,
                sprache);
        return this.domainValueMapper.map(domainCacheEntry, sprache);
    }
}
