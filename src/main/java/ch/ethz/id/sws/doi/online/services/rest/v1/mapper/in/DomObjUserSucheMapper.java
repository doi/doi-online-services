package ch.ethz.id.sws.doi.online.services.rest.v1.mapper.in;

import java.util.List;

import org.mapstruct.Mapper;

import ch.ethz.id.sws.doi.commons.Sorting;
import ch.ethz.id.sws.doi.commons.services.user.DomObjUserSuche;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.ResultSortOrder;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.UserSucheIN;

@Mapper(componentModel = "spring")
public abstract class DomObjUserSucheMapper {

    public abstract DomObjUserSuche map(UserSucheIN userSucheIN);

    public abstract List<Sorting> map(List<ResultSortOrder> resultSortOrderList);

    protected Sorting map(ResultSortOrder resultSortOrder) {
        Sorting sorting = new Sorting();

        switch (resultSortOrder.getAttributeName()) {
            case "id":
                sorting.setColumn("id");
                break;
            case "firstname":
                sorting.setColumn("firstname");
                break;
            case "lastname":
                sorting.setColumn("lastname");
                break;
            case "unique-id":
                sorting.setColumn("uniqueId");
                break;
            case "institution":
                sorting.setColumn("institution");
                break;
            case "email":
                sorting.setColumn("email");
                break;
            case "admin":
                sorting.setColumn("admin");
                break;
            case "pool-id":
                sorting.setColumn("doiPoolId");
                break;
            case "pool-name":
                sorting.setColumn("doiPoolName");
                break;
            case "creation-date":
                sorting.setColumn("creationDate");
                break;
            case "modification-date":
                sorting.setColumn("modificationDate");
                break;
            case "modifying-user":
                sorting.setColumn("modifyingUser");
                break;
        }
        sorting.setAscending(resultSortOrder.getAscending());

        return sorting;
    }
}
