package ch.ethz.id.sws.doi.online.services.rest.v1.model.out;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Errors {

    private List<ErrorOUT> doiErrorList = null;
    private Long totalResultCount = null;

    public Errors(List<ErrorOUT> errorList) {
        this.doiErrorList = errorList;
    }

    @JsonProperty("total")
    public Long getTotalResultCount() {
        return this.totalResultCount;
    }

    public void setTotalResultCount(Long totalResultCount) {
        this.totalResultCount = totalResultCount;
    }

    @JsonProperty("error-array")
    public List<ErrorOUT> getDoiErrorList() {
        return this.doiErrorList;
    }

    public void setDoiErrorList(List<ErrorOUT> doiErrorList) {
        this.doiErrorList = doiErrorList;
    }

}
