package ch.ethz.id.sws.doi.online.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import ch.ethz.id.sws.base.commons.SpringProfile;
import ch.ethz.id.sws.security.commons.LogoutSuccessForwarderHandler;
import ch.ethz.id.sws.security.commons.ShibbolethAuthTokenHeaderFilter;

@Configuration
@EnableWebMvc
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
@ComponentScan(basePackages = "ch.ethz.id.sws.base.login")
@Order(20)
@Profile(value = SpringProfile.PROFILE_WLS)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private final ShibbolethAuthTokenHeaderFilter filter = null;

    @Override
    protected void configure(final HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf().disable()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/loggingout"))
                .logoutSuccessHandler(new LogoutSuccessForwarderHandler())
                .and()
                .addFilter(this.filter)
                .authorizeRequests()
                .antMatchers("/*.view")
                .authenticated()
                .and()
                .formLogin()
                .loginPage("/login.view")
                .permitAll();
    }
}
