package ch.ethz.id.sws.doi.online.services.rest.v1.model.in;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class PoolIN {

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("doi-prefix")
    private String doiPrefix = null;

    @JsonProperty("url-prefix")
    private String urlPrefix = null;

    @JsonProperty("server-url")
    private String serverUrl = null;

    @JsonProperty("from-date-pattern")
    private String fromDatePattern = null;

    @JsonProperty("metadata-prefix")
    private String metadataPrefix = null;

    @JsonProperty("set-name")
    private String setName = null;

    @JsonProperty("default-restype-general-code")
    private Integer defaultResourceTypeGeneralCode = null;

    @JsonProperty("xslt")
    private String xslt = null;

    @JsonProperty("cron-schedule")
    private String harvestCronSchedule = null;

    @JsonProperty("cron-disabled")
    private Integer harvestCronDisabled = null;

    @JsonProperty("import-type-code")
    private Integer importTypeCode = null;

    @JsonProperty("datacite-username")
    private String dataCiteUsername = null;

    @JsonProperty("datacite-password")
    private String dataCitePassword = null;
    
    @JsonProperty("doi-tombstone")
    private String doiTombstone = null;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDoiPrefix() {
        return this.doiPrefix;
    }

    public void setDoiPrefix(String doiPrefix) {
        this.doiPrefix = doiPrefix;
    }

    public String getUrlPrefix() {
        return this.urlPrefix;
    }

    public void setUrlPrefix(String urlPrefix) {
        this.urlPrefix = urlPrefix;
    }

    public String getServerUrl() {
        return this.serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getFromDatePattern() {
        return this.fromDatePattern;
    }

    public void setFromDatePattern(String fromDatePattern) {
        this.fromDatePattern = fromDatePattern;
    }

    public String getMetadataPrefix() {
        return this.metadataPrefix;
    }

    public void setMetadataPrefix(String metadataPrefix) {
        this.metadataPrefix = metadataPrefix;
    }

    public String getSetName() {
        return this.setName;
    }

    public void setSetName(String setName) {
        this.setName = setName;
    }

    public Integer getDefaultResourceTypeGeneralCode() {
        return this.defaultResourceTypeGeneralCode;
    }

    public void setDefaultResourceTypeGeneralCode(Integer defaultResourceTypeGeneralCode) {
        this.defaultResourceTypeGeneralCode = defaultResourceTypeGeneralCode;
    }

    public String getXslt() {
        return this.xslt;
    }

    public void setXslt(String xslt) {
        this.xslt = xslt;
    }

    public String getHarvestCronSchedule() {
        return this.harvestCronSchedule;
    }

    public void setHarvestCronSchedule(String harvestCronSchedule) {
        this.harvestCronSchedule = harvestCronSchedule;
    }

    public Integer getHarvestCronDisabled() {
        return this.harvestCronDisabled;
    }

    public void setHarvestCronDisabled(Integer harvestCronDisabled) {
        this.harvestCronDisabled = harvestCronDisabled;
    }

    public String getDataCiteUsername() {
        return this.dataCiteUsername;
    }

    public void setDataCiteUsername(String dataCiteUsername) {
        this.dataCiteUsername = dataCiteUsername;
    }

    public String getDataCitePassword() {
        return this.dataCitePassword;
    }

    public void setDataCitePassword(String dataCitePassword) {
        this.dataCitePassword = dataCitePassword;
    }

    public Integer getImportTypeCode() {
        return this.importTypeCode;
    }

    public void setImportTypeCode(Integer importTypeCode) {
        this.importTypeCode = importTypeCode;
    }

	public String getDoiTombstone() {
		return doiTombstone;
	}

	public void setDoiTombstone(String doiTombstone) {
		this.doiTombstone = doiTombstone;
	}

}
