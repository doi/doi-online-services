package ch.ethz.id.sws.doi.online.services.rest.v1.mapper.in;

import java.util.List;

import org.mapstruct.Mapper;

import ch.ethz.id.sws.doi.commons.Sorting;
import ch.ethz.id.sws.doi.commons.services.doihistory.DomObjDOIHistorySuche;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.HistorySucheIN;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.ResultSortOrder;

@Mapper(componentModel = "spring")
public abstract class DomObjDOIHistorySucheMapper {

    public abstract DomObjDOIHistorySuche map(HistorySucheIN historySucheIN);

    public abstract List<Sorting> map(List<ResultSortOrder> resultSortOrderList);

    protected Sorting map(ResultSortOrder resultSortOrder) {
        Sorting sorting = new Sorting();

        switch (resultSortOrder.getAttributeName()) {
            case "id":
                sorting.setColumn("id");
                break;
            case "doi-id":
                sorting.setColumn("doiId");
                break;
            case "doi":
                sorting.setColumn("doi");
                break;
            case "url":
                sorting.setColumn("url");
                break;
            case "pool-id":
                sorting.setColumn("doiPoolId");
                break;
            case "pool-name":
                sorting.setColumn("doiPoolName");
                break;
            case "metadata-json":
                sorting.setColumn("metadataJson");
                break;
            case "import-date":
                sorting.setColumn("importDate");
                break;
            case "export-date":
                sorting.setColumn("exportDate");
                break;
            case "creation-date":
                sorting.setColumn("creationDate");
                break;
            case "modification-date":
                sorting.setColumn("modificationDate");
                break;
            case "modifying-user":
                sorting.setColumn("modifyingUser");
                break;
        }
        sorting.setAscending(resultSortOrder.getAscending());

        return sorting;
    }
}
