package ch.ethz.id.sws.doi.online.services.rest.v1.mapper.in;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import ch.ethz.id.sws.doi.commons.services.user.DomObjUser;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.UserIN;

@Mapper(componentModel = "spring")
public abstract class DomObjUserMapper {

    @Mappings({
            @Mapping(target = "doiPoolList", ignore = true),
            @Mapping(target = "creationDate", ignore = true),
            @Mapping(target = "modificationDate", ignore = true),
            @Mapping(target = "modifyingUser", ignore = true),
            @Mapping(target = "admin", source = "admin", defaultValue = "0"),
    })
    public abstract DomObjUser map(UserIN userIN);
    
    @AfterMapping
    protected void afterMapping(@MappingTarget DomObjUser domObjUser, UserIN userIN)
    {
    	if(userIN.getUniqueId() != null)
    	{
    		domObjUser.setUniqueId(userIN.getUniqueId().trim());
    	}
    }
}
