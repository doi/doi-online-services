package ch.ethz.id.sws.doi.online.services.rest.v1.model.out;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class DOIs {

    private List<DOIOUT> doiList = null;
    private Long totalResultCount = null;

    public DOIs(List<DOIOUT> doiList) {
        this.doiList = doiList;
    }

    @JsonProperty("doi-array")
    public List<DOIOUT> getDoiList() {
        return this.doiList;
    }

    public void setDoiList(List<DOIOUT> doiList) {
        this.doiList = doiList;
    }

    @JsonProperty("total")
    public Long getTotalResultCount() {
        return this.totalResultCount;
    }

    public void setTotalResultCount(Long totalResultCount) {
        this.totalResultCount = totalResultCount;
    }
}
