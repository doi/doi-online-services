package ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out;

import java.util.List;

import org.mapstruct.Mapper;

import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOIResultat;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.DOIOUT;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.DOIs;

@Mapper(componentModel = "spring")
public abstract class DOIsMapper {

    public abstract DOIs map(DomObjDOIResultat domObjDOIResultat);

    public abstract List<DOIOUT> map(List<DomObjDOI> domObjDOIList);

    public abstract DOIOUT map(DomObjDOI domObjDOI);
}
