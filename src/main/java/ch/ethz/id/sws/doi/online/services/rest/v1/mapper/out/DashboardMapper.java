package ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;

import ch.ethz.id.sws.base.commons.Sprache;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPoolDashboard;
import ch.ethz.id.sws.doi.commons.services.domains.DOIDomainDAO;
import ch.ethz.id.sws.doi.commons.services.domains.DomainCacheLoaderDOIDomain;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.DashboardOUT;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheService;

@Mapper(componentModel = "spring", imports = { DomainCacheService.class, Sprache.class, DOIDomainDAO.class })
public abstract class DashboardMapper {

    @Autowired
    protected DOIDomainDAO doiDomainDAO = null;

    @Autowired
    protected DomainCacheService domainCacheService = null;

    @PostConstruct
    protected void init() {
        this.domainCacheService.addCustomDomain(DOIDomainDAO.DOM_BATCH_STATUS,
                new DomainCacheLoaderDOIDomain(this.doiDomainDAO));
    }

    @Mappings({
            @Mapping(target = "batchStatus", expression = "java(domainCacheService.getDomainLongTextForKey(DOIDomainDAO.DOM_BATCH_STATUS, Sprache.English, domObjDOIPoolDashboard.getBatchStatusCode()))"),
            @Mapping(target = "hasCredentials", ignore = true),
            @Mapping(target = "hasManualBatchPending", ignore = true),
    })
    public abstract DashboardOUT map(DomObjDOIPoolDashboard domObjDOIPoolDashboard);

    @AfterMapping
    protected void afterDashboardOUTMapping(@MappingTarget DashboardOUT dashboardOUT,
            DomObjDOIPoolDashboard domObjDOIPoolDashboard) {

        if (StringUtils.isNotEmpty(domObjDOIPoolDashboard.getDataCiteUsername()) &&
                domObjDOIPoolDashboard.getDataCitePassword() != null &&
                domObjDOIPoolDashboard.getDataCitePassword().length > 0) {
            dashboardOUT.setHasCredentials(true);
        } else {
            dashboardOUT.setHasCredentials(false);
        }

        if (StringUtils.isNotEmpty(domObjDOIPoolDashboard.getServerUrl())) {
            dashboardOUT.setHasServerConfig(true);
        } else {
            dashboardOUT.setHasServerConfig(false);
        }

        if (StringUtils.isNotEmpty(domObjDOIPoolDashboard.getManualBatchOrder())) {
            dashboardOUT.setHasManualBatchPending(true);
        } else {
            dashboardOUT.setHasManualBatchPending(false);
        }
    }

    public abstract List<DashboardOUT> map(List<DomObjDOIPoolDashboard> DomObjDOIPoolDashboardList);
}
