package ch.ethz.id.sws.doi.online.services.rest;

import java.net.URI;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import ch.ethz.id.sws.base.commons.Sprache;
import ch.ethz.id.sws.base.services.rest.RestException;
import ch.ethz.id.sws.base.services.rest.v1.model.out.RestError;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.commons.services.user.DomObjUser;
import ch.ethz.id.sws.security.authorization.MyUserDetails;

public class RestCommons {
    private static final Log LOG = LogFactory.getLog(RestCommons.class);

    public static Sprache validateLanguage(final String language) throws RestException {
        if (Sprache.fromISOCode(language) != Sprache.English && Sprache.fromISOCode(language) != Sprache.German) {
            RestError.throwException(LOG, null, new Object[] { language }, RestErrorCodes.GLOBAL_PARAM_LANG_INVALID);
        }

        return Sprache.fromISOCode(language);
    }

    public static String getLocation(final String pathPart, final String queryPart) {
        String url = ServletUriComponentsBuilder.fromCurrentRequest().path("/" + pathPart).query(queryPart)
                .toUriString();

        try {
            final URI myUrl = new URI(url);

            // We access the application server directly on DEV. Otherwise we use a apache reverse
            // proxy, which forces us to modify the url accordingly.
            if (!"localhost".equalsIgnoreCase(myUrl.getHost()) && !"127.0.0.1".equalsIgnoreCase(myUrl.getHost())) {
                url = url.replace("http://", "https://");
            }
        } catch (final Exception e) {
        }

        return url;
    }

    public static String getUniqueId(Authentication authentication) {
        if (authentication == null) {
            return null;
        }

        MyUserDetails userDetails = (MyUserDetails) authentication.getDetails();

        if (userDetails != null) {
            return userDetails.getUniqueId();
        }

        return null;
    }

    public static boolean isAdmin(DomObjUser domObjUser) {
        if (domObjUser == null) {
            return false;
        }

        if (domObjUser.getAdmin() > 0) {
            return true;
        }

        return false;
    }

    public static boolean isAuthorizedForPool(DomObjUser domObjUser, long doiPoolId) {
        if (domObjUser == null) {
            return false;
        }

        if (domObjUser.getAdmin() > 0) {
            return true;
        }

        List<DomObjDOIPool> doiPoolList = domObjUser.getDoiPoolList();

        if (doiPoolList != null && !doiPoolList.isEmpty()) {
            for (DomObjDOIPool domObjDOIPool : doiPoolList) {
                if (domObjDOIPool.getId() == doiPoolId) {
                    return true;
                }
            }
        }

        return false;
    }
}
