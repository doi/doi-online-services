package ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out;

import java.util.List;

import org.mapstruct.Mapper;

import ch.ethz.id.sws.doi.commons.services.user.DomObjUser;
import ch.ethz.id.sws.doi.commons.services.user.DomObjUserResultat;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.UserOUT;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.Users;

@Mapper(componentModel = "spring", uses = PoolsMapper.class)
public abstract class UsersMapper {

    public abstract Users map(DomObjUserResultat domObjUserResultat);

    public abstract List<UserOUT> map(List<DomObjUser> DomObjUserList);

    public abstract UserOUT map(DomObjUser domObjUser);
}
