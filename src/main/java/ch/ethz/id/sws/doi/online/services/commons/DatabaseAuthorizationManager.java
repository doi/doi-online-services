package ch.ethz.id.sws.doi.online.services.commons;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import ch.ethz.id.sws.doi.commons.services.user.DomObjUser;
import ch.ethz.id.sws.doi.commons.services.user.UserDAO;
import ch.ethz.id.sws.doi.online.services.RootConfig;
import ch.ethz.id.sws.security.authorization.AuthorizationManager;
import ch.ethz.id.sws.security.authorization.MyUserDetails;
import ch.ethz.id.sws.security.authorization.role.RoleManager;

public class DatabaseAuthorizationManager implements AuthorizationManager {

    @Autowired
    private final UserDAO userDAO = null;

    @Autowired
    private final RoleManager roleManager = null;

    @Override
    public void determineAuthorization(final MyUserDetails userDetails, final HttpServletRequest request)
            throws Exception {
        final List<String> rolesAndGrantsList = new ArrayList<String>();
        final DomObjUser domObjUser = this.attemptDBAuthorization(userDetails);

        if (domObjUser != null) {
            userDetails.setUsername(domObjUser.getUniqueId());
            userDetails.setSurname(domObjUser.getFirstname());
            userDetails.setGivenName(domObjUser.getLastname());
            userDetails.setHomeOrg(domObjUser.getInstitution());
            userDetails.setEmail(domObjUser.getEmail());

            if (StringUtils.isEmpty(domObjUser.getFirstname())
                    && StringUtils.isEmpty(domObjUser.getLastname())) {
                userDetails.setSurname(userDetails.getSurname());
                userDetails.setGivenName(userDetails.getGivenName());
            }

            if (domObjUser.getAdmin() == 1) {
                rolesAndGrantsList.add(RootConfig.AUTH_DOI_ROLE_ADMIN);
            } else {
                rolesAndGrantsList.add(RootConfig.AUTH_DOI_ROLE_USER);
            }

            if (this.roleManager != null) {
                this.roleManager.validateRoles(rolesAndGrantsList);
            }
        }

        // Add roles to granted authorities
        for (final String role : rolesAndGrantsList) {
            userDetails.addGrantedAuthority(new SimpleGrantedAuthority(role));
        }
    }

    private DomObjUser attemptDBAuthorization(final MyUserDetails userDetails) {
        return this.userDAO.getUserByUniqueId(userDetails.getUniqueId());
    }
}
