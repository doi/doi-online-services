package ch.ethz.id.sws.doi.online.services.rest.v1.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.ethz.id.sws.base.services.rest.v1.model.out.RestError;
import ch.ethz.id.sws.doi.commons.services.doierror.DOIErrorService;
import ch.ethz.id.sws.doi.commons.services.doierror.DomObjDOIError;
import ch.ethz.id.sws.doi.commons.services.doierror.DomObjDOIErrorResultat;
import ch.ethz.id.sws.doi.commons.services.doierror.DomObjDOIErrorSuche;
import ch.ethz.id.sws.doi.commons.services.user.DomObjUser;
import ch.ethz.id.sws.doi.commons.services.user.UserService;
import ch.ethz.id.sws.doi.online.services.rest.RestCommons;
import ch.ethz.id.sws.doi.online.services.rest.RestErrorCodes;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.in.DomObjDOIErrorSucheMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out.ErrorsMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.ErrorIN;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.ErrorSucheIN;

@CrossOrigin
@RestController
@RequestMapping(value = "/v1/errors")
public class DOIErrorController {

    private static Log LOG = LogFactory.getLog(DOIErrorController.class);

    @Autowired
    private final UserService userService = null;

    @Autowired
    private final DOIErrorService doiErrorService = null;

    @Autowired
    private final ErrorsMapper errorsMapper = null;

    @Autowired
    private final DomObjDOIErrorSucheMapper domObjDOIErrorSucheMapper = null;

    @ResponseBody
    @GetMapping(value = "/{errorId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getError(@PathVariable final long errorId, final Authentication authentication) throws Exception {
        LOG.debug("REST getError: " + errorId);

        try {
            final DomObjDOIError domObjDOIError = this.doiErrorService.getDOIError(errorId);

            if (domObjDOIError == null) {
                RestError.throwException(LOG, "Pool", new Object[] { errorId },
                        RestErrorCodes.GLOBAL_ELEMENT_NOTFOUND);
            }

            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAuthorizedForPool(domObjUser, domObjDOIError.getDoiPoolId())) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            return this.errorsMapper.map(domObjDOIError);
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PostMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object searchError(@RequestBody final ErrorSucheIN errorSucheIN, final Authentication authentication)
            throws Exception {
        LOG.debug("REST searchError: " + errorSucheIN);

        try {
            final DomObjDOIErrorSuche domObjDOIErrorSuche = this.domObjDOIErrorSucheMapper.map(errorSucheIN);

            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAdmin(domObjUser)) {
                domObjDOIErrorSuche.setUserId(domObjUser.getId());
            }

            final DomObjDOIErrorResultat domObjDOIErrorResultat = this.doiErrorService
                    .searchDOIError(domObjDOIErrorSuche);

            return this.errorsMapper.map(domObjDOIErrorResultat);
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PutMapping(value = "/{errorId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object updateError(@PathVariable final long errorId, @RequestBody final ErrorIN errorIN,
            final HttpServletResponse response, final Authentication authentication) throws Exception {
        LOG.debug("REST updateError: " + errorId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            final DomObjDOIError domObjDOIError = this.doiErrorService.getDOIError(errorId);

            if (!RestCommons.isAuthorizedForPool(domObjUser, domObjDOIError.getDoiPoolId())) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            domObjDOIError.setComment(errorIN.getComment());
            domObjDOIError.setHandled(errorIN.getHandled());
            this.doiErrorService.updateDOIError(domObjDOIError);

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }
}
