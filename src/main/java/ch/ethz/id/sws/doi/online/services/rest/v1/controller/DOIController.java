package ch.ethz.id.sws.doi.online.services.rest.v1.controller;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.ethz.id.sws.base.services.rest.v1.model.out.RestError;
import ch.ethz.id.sws.doi.commons.services.doi.DOIService;
import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOIResultat;
import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOISuche;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.commons.services.user.DomObjUser;
import ch.ethz.id.sws.doi.commons.services.user.UserService;
import ch.ethz.id.sws.doi.online.services.rest.RestCommons;
import ch.ethz.id.sws.doi.online.services.rest.RestErrorCodes;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.in.DomObjDOIMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.in.DomObjDOISucheMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out.DOIsMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.DOIIN;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.DOISucheIN;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.ResultOUT;

@CrossOrigin
@RestController
@RequestMapping(value = "/v1")
public class DOIController {
    private static Log LOG = LogFactory.getLog(DOIController.class);

    @Autowired
    private final UserService userService = null;

    @Autowired
    private final DOIPoolService doiPoolService = null;

    @Autowired
    private final DOIsMapper doisMapper = null;

    @Autowired
    private final DomObjDOIMapper domObjDOIMapper = null;

    @Autowired
    private final DomObjDOISucheMapper domObjDOISucheMapper = null;

    @Autowired
    private final DOIService doiService = null;

    @ResponseBody
    @PostMapping(value = "/pools/{poolId}/dois", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object createDOI(@RequestBody final DOIIN doiIN, @PathVariable final long poolId,
            final HttpServletResponse response,
            final Authentication authentication) throws Exception {
        LOG.debug("REST createDOI: " + doiIN);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAuthorizedForPool(domObjUser, poolId)) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            DomObjDOIPool domObjDOIPool = this.doiPoolService.getDOIPool(doiIN.getDoiPoolId());
            final DomObjDOI domObjDOI = this.domObjDOIMapper.map(doiIN, domObjDOIPool);
            domObjDOI.setImportDate(LocalDateTime.now());

            try {
                this.doiService.insertDOI(domObjDOI);
            } catch (DuplicateKeyException e) {
                RestError.throwException(LOG, null, new Object[] { "doi", domObjDOI.getDoi() },
                        RestErrorCodes.GLOBAL_ELEMENT_ALREADYEXISTS);
            }

            response.setHeader("Location", RestCommons.getLocation(Long.toString(domObjDOI.getId()), "lang=de"));
            response.setStatus(HttpStatus.CREATED.value());

            return new ResultOUT(domObjDOI.getId());
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @GetMapping(value = "/dois/{doiId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getDOI(@PathVariable final long doiId, final Authentication authentication) throws Exception {
        LOG.debug("REST getDOI: " + doiId);

        try {
            final DomObjDOI domObjDOI = this.doiService.getDOI(doiId);

            if (domObjDOI == null) {
                RestError.throwException(LOG, "DOI", new Object[] { doiId },
                        RestErrorCodes.GLOBAL_ELEMENT_NOTFOUND);
            }

            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAuthorizedForPool(domObjUser, domObjDOI.getDoiPoolId())) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            return this.doisMapper.map(domObjDOI);
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PostMapping(value = "/dois/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object searchDOI(@RequestBody final DOISucheIN doiSucheIN, final Authentication authentication)
            throws Exception {
        LOG.debug("REST searchDOI: " + doiSucheIN);

        try {
            final DomObjDOISuche domObjDOISuche = this.domObjDOISucheMapper.map(doiSucheIN);

            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAdmin(domObjUser)) {
                domObjDOISuche.setUserId(domObjUser.getId());
            }

            final DomObjDOIResultat domObjDOIResultat = this.doiService.searchDOI(domObjDOISuche);

            return this.doisMapper.map(domObjDOIResultat);
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PutMapping(value = "/dois/{doiId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object updateDOI(@PathVariable final long doiId,
            @RequestBody final DOIIN doiIN,
            final HttpServletResponse response, final Authentication authentication) throws Exception {
        LOG.debug("REST updateDOI: " + doiId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            final DomObjDOI domObjDOIOld = this.doiService.getDOI(doiId);

            if (!RestCommons.isAuthorizedForPool(domObjUser, domObjDOIOld.getDoiPoolId())) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            DomObjDOIPool domObjDOIPool = this.doiPoolService.getDOIPool(doiIN.getDoiPoolId());
            final DomObjDOI domObjDOI = this.domObjDOIMapper.map(doiIN, domObjDOIPool);
            domObjDOI.setImportDate(LocalDateTime.now());
            domObjDOI.setId(doiId);

            try {
                this.doiService.updateDOI(domObjDOI);
            } catch (DuplicateKeyException e) {
                RestError.throwException(LOG, null, new Object[] { "doi", domObjDOI.getDoi() },
                        RestErrorCodes.GLOBAL_ELEMENT_ALREADYEXISTS);
            }

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @DeleteMapping(value = "/dois/{doiId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object deleteDOI(@PathVariable final long doiId,
            final HttpServletResponse response,
            final Authentication authentication) throws Exception {
        LOG.debug("REST deleteDOI: " + doiId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            final DomObjDOI domObjDOIOld = this.doiService.getDOI(doiId);

            if (!RestCommons.isAuthorizedForPool(domObjUser, domObjDOIOld.getDoiPoolId())) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            this.doiService.deleteDOI(doiId);

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @DeleteMapping(value = "/dois", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object deleteDOIs(@RequestParam(value = "ids", required = true) Long[] idArray,
            final HttpServletResponse response,
            final Authentication authentication) throws Exception {
        LOG.debug("REST deleteDOIs: " + idArray);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);

            for (Long doiId : idArray) {
                DomObjDOI domObjDOI = this.doiService.getDOI(doiId);

                if (!RestCommons.isAuthorizedForPool(domObjUser, domObjDOI.getDoiPoolId())) {
                    RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
                }
            }

            for (Long doiId : idArray) {
                this.doiService.deleteDOI(doiId);
            }

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }
}
