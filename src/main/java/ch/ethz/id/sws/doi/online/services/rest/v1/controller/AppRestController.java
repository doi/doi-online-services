package ch.ethz.id.sws.doi.online.services.rest.v1.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.ethz.id.sws.base.commons.version.VersionManager;
import ch.ethz.id.sws.doi.online.services.RootConfig;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.UserMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.ClientLogEntry;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.User;
import ch.ethz.id.sws.security.commons.ShibbolethAuthToken;
import ch.ethz.id.sws.security.commons.ShibbolethUserDetails;

@CrossOrigin
@RestController
@PreAuthorize("hasAnyAuthority('" + RootConfig.AUTH_DOI_ROLE_ADMIN + "','"
        + RootConfig.AUTH_DOI_ROLE_USER + "')")
@RequestMapping(value = "/v1/app")
public class AppRestController {
    private static Log LOG = LogFactory.getLog(AppRestController.class);
    private static Log CLIENTLOG = LogFactory.getLog("ClientLog");

    @Autowired
    private final UserMapper userMapper = null;

    @Autowired
    private final VersionManager versionManager = null;

    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    @ResponseBody
    @GetMapping(value = "/whoami", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object whoAmI(final Authentication authentication) throws Exception {
        LOG.debug("REST whoami: " + authentication.getName());

        try {
            final User user = this.userMapper.map((ShibbolethUserDetails) authentication.getDetails(),
                    (ShibbolethAuthToken) authentication);
            user.getServerVersion().setAppStartupTime(this.versionManager.getAppStartupTime());

            return user;
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PostMapping(value = "/log")
    public void log(@RequestBody final ClientLogEntry clientLogEntry, final Authentication authentication)
            throws Exception {
        try {
            String message = "";

            if (!ObjectUtils.isEmpty(clientLogEntry.getTimestamp())) {
                try {
                    message = message + this.dateFormat.format(new Date(clientLogEntry.getTimestamp())) + " ";
                } catch (final Exception e) {
                }
            }
            if (!StringUtils.isEmpty(clientLogEntry.getMessage())) {
                message = message + clientLogEntry.getMessage();
            }
            if (!ObjectUtils.isEmpty(clientLogEntry.getLine())) {
                message = message + " (" + clientLogEntry.getLine();

                if (!ObjectUtils.isEmpty(clientLogEntry.getColumn())) {
                    message = message + ", " + clientLogEntry.getColumn() + "): \n";
                }
            }
            if (!StringUtils.isEmpty(clientLogEntry.getStacktrace())) {
                message = message + clientLogEntry.getStacktrace();
            }
            if (!StringUtils.isEmpty(clientLogEntry.getClient())) {
                message = message + clientLogEntry.getClient();
            }
            if (!StringUtils.isEmpty(clientLogEntry.getBrowser())) {
                message = message + ";" + clientLogEntry.getBrowser();
            }
            if (!StringUtils.isEmpty(clientLogEntry.getVersion())) {
                message = message + ";" + clientLogEntry.getVersion();
            }

            switch (clientLogEntry.getSeverity().toLowerCase()) {
                case "error":
                    CLIENTLOG.error(message);
                    break;
                case "warning":
                    CLIENTLOG.warn(message);
                    break;
                case "info":
                    CLIENTLOG.info(message);
                    break;
                case "debug":
                    CLIENTLOG.debug(message);
                    break;
                default:
                    CLIENTLOG.trace(message);
                    break;
            }
        } catch (final Throwable t) {
            throw t;
        }
    }
}
