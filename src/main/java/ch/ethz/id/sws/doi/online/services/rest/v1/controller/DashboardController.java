package ch.ethz.id.sws.doi.online.services.rest.v1.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter.SseEventBuilder;

import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.commons.services.user.DomObjUser;
import ch.ethz.id.sws.doi.commons.services.user.UserService;
import ch.ethz.id.sws.doi.online.services.commons.MySseEmitter;
import ch.ethz.id.sws.doi.online.services.rest.RestCommons;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out.DashboardMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.DashboardOUT;

@CrossOrigin
@RestController
@RequestMapping(value = "/v1/pools")
public class DashboardController {
    private static Log LOG = LogFactory.getLog(DashboardController.class);

    @Autowired
    private final DashboardMapper dashboardMapper = null;

    @Autowired
    private final DOIPoolService doiPoolService = null;

    @Autowired
    private final UserService userService = null;

    private static final List<MySseEmitter> sseEmitterList = Collections
            .synchronizedList(new ArrayList<MySseEmitter>());

    @ResponseBody
    @GetMapping(value = "/dashboard", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getDashboard(final Authentication authentication) throws Exception {
        LOG.debug("REST getDashboard: ");

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAdmin(domObjUser)) {
                return this.dashboardMapper.map(this.doiPoolService.getDashboard(domObjUser.getId()));
            }

            return this.dashboardMapper.map(this.doiPoolService.getDashboard(null));
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @GetMapping(value = "/dashboard/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter streamDashboard(final Authentication authentication, final HttpServletResponse response)
            throws Exception {
        LOG.debug("REST streamDashboard: ");
        response.setHeader("Cache-Control", "no-store");

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            List<DashboardOUT> dashboardOUTList = null;

            if (!RestCommons.isAdmin(domObjUser)) {
                dashboardOUTList = this.dashboardMapper.map(this.doiPoolService.getDashboard(domObjUser.getId()));
            } else {
                dashboardOUTList = this.dashboardMapper.map(this.doiPoolService.getDashboard(null));
            }

            final MySseEmitter sseEmitter = new MySseEmitter(dashboardOUTList);
            final SseEventBuilder refreshEvent = SseEmitter.event()
                    .name("refresh-dashboard-status")
                    .data(dashboardOUTList);
            sseEmitter.send(refreshEvent);
            this.addToSseEmitters(sseEmitter);
            sseEmitter.onCompletion(() -> this.onSseEmitterEvent(sseEmitter, null));
            sseEmitter.onTimeout(() -> this.onSseEmitterEvent(sseEmitter, null));
            sseEmitter.onError((Throwable t) -> this.onSseEmitterEvent(sseEmitter, t));

            return sseEmitter;
        } catch (final Throwable t) {
            throw t;
        }
    }

    private void addToSseEmitters(MySseEmitter sseEmitter) {
        DashboardController.sseEmitterList.add(sseEmitter);

        if (DashboardController.sseEmitterList.size() == 1) {
            Timer timer = new Timer("SSE dashboard update timer");
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    if (DashboardController.sseEmitterList.isEmpty()) {
                        timer.cancel();
                    }

                    DashboardController.this.updateEmitters();
                }
            };

            timer.scheduleAtFixedRate(task, 4000L, 5000L);
        }
    }

    private void updateEmitters() {
        List<MySseEmitter> sseEmitterErrorList = new ArrayList<MySseEmitter>();
        List<DashboardOUT> dashboardList = this.dashboardMapper.map(this.doiPoolService.getDashboard(null));

        synchronized (DashboardController.sseEmitterList) {
            for (MySseEmitter mySseEmitter : DashboardController.sseEmitterList) {
                try {
                    final SseEventBuilder refreshEvent = SseEmitter.event()
                            .name("refresh-dashboard-status")
                            .data(mySseEmitter.filter(dashboardList));

                    mySseEmitter.send(refreshEvent);
                } catch (final Exception e) {
                    sseEmitterErrorList.add(mySseEmitter);
                }
            }
        }

        for (MySseEmitter mySseEmitter : sseEmitterErrorList) {
            this.onSseEmitterEvent(mySseEmitter, null);
        }
    }

    private void onSseEmitterEvent(MySseEmitter sseEmitter, Throwable t) {
        DashboardController.sseEmitterList.remove(sseEmitter);

        if (t != null) {
            LOG.info("Exception during SSE transmission: ", t);
        } else {
            LOG.info("Closing SSE transmission due to general error.");
        }
    }
}
