package ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out;

import java.nio.charset.StandardCharsets;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPoolResultat;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.PoolOUT;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.Pools;

@Mapper(componentModel = "spring")
public abstract class PoolsMapper {

    public abstract Pools map(DomObjDOIPoolResultat domObjDOIPoolResultat);

    public abstract List<PoolOUT> map(List<DomObjDOIPool> DomObjDOIPoolList);

    @Mappings({
            @Mapping(target = "dataCitePassword", expression = "java(decryptPassword(domObjDOIPool))")
    })
    public abstract PoolOUT map(DomObjDOIPool domObjDOIPool) throws Exception;

    protected String decryptPassword(DomObjDOIPool domObjDOIPool) throws Exception {
        if (domObjDOIPool.getDataCitePassword() != null) {
            return new String(domObjDOIPool.getDataCitePasswordAsPlainText(), StandardCharsets.UTF_8);
        }

        return null;
    }
}
