package ch.ethz.id.sws.doi.online.services;

import java.io.IOException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import ch.ethz.id.sws.base.commons.BaseCommonsConfig;
import ch.ethz.id.sws.base.commons.SpringProfile;
import ch.ethz.id.sws.base.commons.context.IDWebProperties;
import ch.ethz.id.sws.doi.commons.DOICommonsConfig;
import ch.ethz.id.sws.doi.online.services.commons.DatabaseAuthorizationManager;
import ch.ethz.id.sws.security.IAMSecurityLibConfig;
import ch.ethz.id.sws.security.authorization.AuthorizationManager;
import ch.ethz.id.sws.security.authorization.role.Role;
import ch.ethz.id.sws.security.authorization.role.RoleModel;

@Configuration
@EnableTransactionManagement
@Import({ BaseCommonsConfig.class,
        IAMSecurityLibConfig.class,
        DOICommonsConfig.class })
@Profile(value = SpringProfile.PROFILE_WLS)
public class RootConfig {

    private static Log LOG = LogFactory.getLog(RootConfig.class);

    public final static String AUTH_DOI_ROLE_ADMIN = "DOI-Role-Admin";
    public final static String AUTH_DOI_ROLE_USER = "DOI-Role-User";

    @Value("#{systemProperties['ch.ethz.id.bi.b3.lb.pathToSettingFiles']}")
    private String settingsPath;

    // The argument 'destroyMethod' is required due to WLS container
    @Bean(name = "dataSource", destroyMethod = "")
    public DataSource getDataSource() {
        final String jndiName = "java:comp/env/jdbc/doiolsrvDS";

        try {
            final JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
            return dataSourceLookup.getDataSource(jndiName);
        } catch (final Exception e) {
            LOG.error("Could not lookup JNDI name '"
                    + jndiName + "': ", e);
            throw e;
        }
    }

    @Bean(name = "idProperties")
    public IDWebProperties getIdProperties() throws IOException {
        return new IDWebProperties(this.settingsPath + "/doionlinesrv/doiOnlineServices.properties");
    }

    @Bean(name = "messageSource")
    public ReloadableResourceBundleMessageSource getMessageSource() {
        final ReloadableResourceBundleMessageSource msgSource = new ReloadableResourceBundleMessageSource();

        msgSource.addBasenames("classpath:messages/rest-errors");
        msgSource.addBasenames("classpath:messages/service-errors");
        msgSource.addBasenames("classpath:messages/messages");
        msgSource.addBasenames("classpath:messages/appmessages");
        msgSource.setDefaultEncoding("UTF-8");

        return msgSource;
    }

    @Bean(name = "roleModel")
    public RoleModel getRoleModel() {
        final Role roleAdmin = new Role(AUTH_DOI_ROLE_ADMIN, 99);
        final Role roleUser = new Role(AUTH_DOI_ROLE_USER, 0);

        roleAdmin.setName("roles.admin");
        roleUser.setName("roles.user");

        final RoleModel roleModel = new RoleModel();
        roleModel.addRole(roleUser);
        roleModel.addRole(roleAdmin);

        return roleModel;
    }

    @Bean(name = "authorizationManager")
    public AuthorizationManager getAuthorizationManager() {
        return new DatabaseAuthorizationManager();
    }
}
