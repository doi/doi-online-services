package ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out;

import java.util.List;

import org.mapstruct.Context;
import org.mapstruct.Mapper;

import ch.ethz.id.sws.base.commons.Sprache;
import ch.ethz.id.sws.doi.commons.services.domains.CacheEntryDomObjDOIDomain;
import ch.ethz.id.sws.doi.commons.services.domains.DomObjDOIDomainValue;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.DomainValue;
import ch.ethz.id.sws.dom.commons.services.domains.DomObjCustomDomain;
import ch.ethz.id.sws.dom.commons.services.domains.DomObjTextAlphaNumeric;
import ch.ethz.id.sws.dom.commons.services.domains.DomObjTextCode;
import ch.ethz.id.sws.dom.commons.services.domains.DomObjTextZahl;
import ch.ethz.id.sws.dom.commons.services.domains.cache.CacheEntryCustom;
import ch.ethz.id.sws.dom.commons.services.domains.cache.CacheEntryDomObjTextAlphaNumeric;
import ch.ethz.id.sws.dom.commons.services.domains.cache.CacheEntryDomObjTextCode;
import ch.ethz.id.sws.dom.commons.services.domains.cache.CacheEntryDomObjTextZahl;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheEntry;

@Mapper(componentModel = "spring")
public abstract class DomainValueMapper {

    public DomainValue map(DomainCacheEntry domainCacheEntry, @Context Sprache sprache) {
        DomainValue domainValue = new DomainValue();

        if (domainCacheEntry instanceof CacheEntryDomObjTextZahl) {
            DomObjTextZahl domObjTextZahl = (DomObjTextZahl) domainCacheEntry.getEntry();

            domainValue.setDescShort(mapStringWithGermanFallback(
                    domObjTextZahl.getTextKurz(),
                    domObjTextZahl.getTextKurzE(), sprache));
            domainValue.setDesc(mapStringWithGermanFallback(
                    domObjTextZahl.getTextLang(),
                    domObjTextZahl.getTextLangE(), sprache));
            domainValue.setActive(domObjTextZahl.getIsActiv());
            domainValue.setCode(domObjTextZahl.getZahlwert());
        } else if (domainCacheEntry instanceof CacheEntryDomObjTextAlphaNumeric) {
            DomObjTextAlphaNumeric domObjTextAlphaNumeric = (DomObjTextAlphaNumeric) domainCacheEntry.getEntry();

            domainValue.setDescShort(mapStringWithGermanFallback(
                    domObjTextAlphaNumeric.getTextKurz(),
                    domObjTextAlphaNumeric.getTextKurzE(), sprache));
            domainValue.setDesc(mapStringWithGermanFallback(
                    domObjTextAlphaNumeric.getTextLang(),
                    domObjTextAlphaNumeric.getTextLangE(), sprache));
            domainValue.setActive(domObjTextAlphaNumeric.getIsActiv());
            domainValue.setCode(domObjTextAlphaNumeric.getTextWert());
        } else if (domainCacheEntry instanceof CacheEntryDomObjTextCode) {
            DomObjTextCode domObjTextCode = (DomObjTextCode) domainCacheEntry.getEntry();

            domainValue.setDescShort(mapStringWithGermanFallback(
                    domObjTextCode.getBezeichnung(),
                    domObjTextCode.getBezeichnungEn(), sprache));
            domainValue.setDesc(mapStringWithGermanFallback(
                    domObjTextCode.getBezeichnung(),
                    domObjTextCode.getBezeichnungEn(), sprache));
            domainValue.setCode(domObjTextCode.getWert());
            domainValue.setActive(domObjTextCode.getAktiv());
        } else if (domainCacheEntry instanceof CacheEntryCustom) {
            DomObjCustomDomain customDomain = (DomObjCustomDomain) domainCacheEntry.getEntry();

            domainValue.setDescShort(mapStringWithGermanFallback(
                    customDomain.getTextKurzDe(),
                    customDomain.getTextKurzEn(), sprache));
            domainValue.setDesc(mapStringWithGermanFallback(
                    customDomain.getTextLangDe(),
                    customDomain.getTextLangEn(), sprache));
            domainValue.setCode(customDomain.getValue());
        } else if (domainCacheEntry instanceof CacheEntryDomObjDOIDomain) {
            DomObjDOIDomainValue customDomain = (DomObjDOIDomainValue) domainCacheEntry.getEntry();

            domainValue.setDescShort(customDomain.getDescription());
            domainValue.setDesc(customDomain.getDescription());
            domainValue.setCode(customDomain.getValue());
        }

        return domainValue;
    }

    public abstract List<DomainValue> mapList(List<DomainCacheEntry> domList, @Context Sprache sprache);

    protected static String mapStringWithGermanFallback(String germanString, String englishString,
            @Context Sprache sprache) {
        if (sprache == Sprache.English) {
            if (englishString != null && englishString.trim().length() > 0) {
                return englishString;
            }
        }

        return germanString;
    }
}
