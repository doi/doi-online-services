package ch.ethz.id.sws.doi.online.services.rest.v1.model.in;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class UserSucheIN {
    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("firstname")
    private String firstname = null;

    @JsonProperty("lastname")
    private String lastname = null;

    @JsonProperty("unique-id")
    private String uniqueId = null;

    @JsonProperty("institution")
    private String institution = null;

    @JsonProperty("email")
    private String email = null;

    @JsonProperty("admin")
    private Integer admin = null;

    @JsonProperty("pool-id")
    private Long doiPoolId = null;

    @JsonProperty("creation-date-start")
    private String creationDateStart = null;

    @JsonProperty("creation-date-end")
    private String creationDateEnd = null;

    @JsonProperty("modification-date-start")
    private String modificationDateStart = null;

    @JsonProperty("modification-date-end")
    private String modificationDateEnd = null;

    @JsonProperty("modifying-user")
    private String modifyingUser = null;

    @JsonProperty("rs-first")
    private Long rsFirst = null;

    @JsonProperty("rs-size")
    private Long rsSize = null;

    @JsonProperty("result-sortorder-array")
    private List<ResultSortOrder> resultSortOrderList = new ArrayList<ResultSortOrder>();

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUniqueId() {
        return this.uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getInstitution() {
        return this.institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAdmin() {
        return this.admin;
    }

    public void setAdmin(Integer admin) {
        this.admin = admin;
    }

    public Long getDoiPoolId() {
        return this.doiPoolId;
    }

    public void setDoiPoolId(Long doiPoolId) {
        this.doiPoolId = doiPoolId;
    }

    public String getCreationDateStart() {
        return this.creationDateStart;
    }

    public void setCreationDateStart(String creationDateStart) {
        this.creationDateStart = creationDateStart;
    }

    public String getCreationDateEnd() {
        return this.creationDateEnd;
    }

    public void setCreationDateEnd(String creationDateEnd) {
        this.creationDateEnd = creationDateEnd;
    }

    public String getModificationDateStart() {
        return this.modificationDateStart;
    }

    public void setModificationDateStart(String modificationDateStart) {
        this.modificationDateStart = modificationDateStart;
    }

    public String getModificationDateEnd() {
        return this.modificationDateEnd;
    }

    public void setModificationDateEnd(String modificationDateEnd) {
        this.modificationDateEnd = modificationDateEnd;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public void setModifyingUser(String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }

    public Long getRsFirst() {
        return this.rsFirst;
    }

    public void setRsFirst(Long rsFirst) {
        this.rsFirst = rsFirst;
    }

    public Long getRsSize() {
        return this.rsSize;
    }

    public void setRsSize(Long rsSize) {
        this.rsSize = rsSize;
    }

    public List<ResultSortOrder> getResultSortOrderList() {
        return this.resultSortOrderList;
    }

    public void setResultSortOrderList(List<ResultSortOrder> resultSortOrderList) {
        this.resultSortOrderList = resultSortOrderList;
    }

}
