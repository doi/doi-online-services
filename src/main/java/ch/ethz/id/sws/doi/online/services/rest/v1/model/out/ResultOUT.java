package ch.ethz.id.sws.doi.online.services.rest.v1.model.out;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class ResultOUT {
    @JsonProperty("id")
    private long id = 0;

    public ResultOUT(final long id) {
        this.id = id;
    }

    public long getId() {
        return this.id;
    }

    public void setId(final long id) {
        this.id = id;
    }
}
