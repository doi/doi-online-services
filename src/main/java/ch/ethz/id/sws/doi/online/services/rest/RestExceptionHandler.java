package ch.ethz.id.sws.doi.online.services.rest;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.context.B3ApplicationContext;
import ch.ethz.id.sws.base.services.rest.RestException;
import ch.ethz.id.sws.base.services.rest.v1.model.out.RestError;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    private static Log LOG = LogFactory.getLog(RestExceptionHandler.class);

    @Autowired
    private final B3ApplicationContext b3ApplicationContext = null;

    @Autowired
    private MessageSource messageSource;

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException exception,
            final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        final RestError restError = new RestError(exception);
        restError.setHttpStatus(HttpStatus.BAD_REQUEST);

        LOG.warn("Request message could not be read: ", exception);
        return this.buildResponse(restError, null);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            final MissingServletRequestParameterException exception, final HttpHeaders headers, final HttpStatus status,
            final WebRequest request) {
        final RestError restError = new RestError(exception);
        restError.setHttpStatus(HttpStatus.BAD_REQUEST);
        restError.setStackTrace(null);

        LOG.warn("Servlet exception occurred: ", exception);
        return this.buildResponse(restError, null);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(final TypeMismatchException exception,
            final HttpHeaders headers,
            final HttpStatus status, final WebRequest request) {
        final RestError restError = new RestError(exception);
        restError.setHttpStatus(HttpStatus.BAD_REQUEST);
        restError.setStackTrace(null);

        LOG.info("Request type mismatch: ", exception);
        return this.buildResponse(restError, null);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    protected ResponseEntity<Object> handleIllegalArgumentException(final IllegalArgumentException exception,
            final HttpServletRequest httpRequest) {
        final RestError restError = new RestError(exception);
        restError.setHttpStatus(HttpStatus.BAD_REQUEST);
        restError.setMessageId(RestErrorCodes.GLOBAL_UNEXPECTED_ERROR);
        restError.setStackTrace(null);

        LOG.error("REST service exception: ", exception);
        return this.buildResponse(restError, httpRequest);
    }

    @ExceptionHandler(UnsatisfiedServletRequestParameterException.class)
    protected ResponseEntity<Object> handleUnsatisfiedServletRequestParameterException(
            final UnsatisfiedServletRequestParameterException exception, final HttpServletRequest httpRequest) {
        final RestError restError = new RestError(exception);
        restError.setHttpStatus(HttpStatus.BAD_REQUEST);
        restError.setMessageId(RestErrorCodes.GLOBAL_UNEXPECTED_ERROR);
        restError.setStackTrace(null);

        LOG.error("REST service exception: ", exception);
        return this.buildResponse(restError, httpRequest);
    }

    @ExceptionHandler(ServiceException.class)
    protected ResponseEntity<Object> handlePubCalMgrServiceException(final ServiceException exception,
            final HttpServletRequest httpRequest) {
        final RestError restError = new RestError(exception);

        final String msg = this.getServiceMessage(exception, Locale.getDefault());
        restError.setMessageId(exception.getErrorCode());
        restError.setMessage(msg);

        switch (exception.getErrorCode()) {
            default:
                restError.setHttpStatus(HttpStatus.BAD_REQUEST);
                break;
        }

        LOG.info(msg, exception);
        return this.buildResponse(restError, httpRequest);
    }

    @ExceptionHandler(RestException.class)
    protected ResponseEntity<Object> handleRestException(final RestException exception,
            final HttpServletRequest httpRequest) {
        final RestError restError = new RestError(exception);
        final String msg = this.getRestMessage(exception, Locale.getDefault());

        switch (exception.getErrorCode()) {
            case RestErrorCodes.GLOBAL_UNEXPECTED_ERROR:
                restError.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                LOG.error(msg, exception);
                break;
            case RestErrorCodes.GLOBAL_ELEMENT_NOTFOUND:
                restError.setHttpStatus(HttpStatus.NOT_FOUND);
                break;
            case RestErrorCodes.GLOBAL_NOT_AUTHORIZED:
                restError.setHttpStatus(HttpStatus.FORBIDDEN);
                break;
            default:
                restError.setHttpStatus(HttpStatus.BAD_REQUEST);
                LOG.info(msg, exception);
                break;
        }

        restError.setMessageId(exception.getErrorCode());
        restError.setMessage(msg);

        return this.buildResponse(restError, httpRequest);
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleGeneralException(final Exception exception,
            final HttpServletRequest httpRequest) {
        final RestError restError = new RestError(exception);
        restError.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        restError.setMessageId(RestErrorCodes.GLOBAL_UNEXPECTED_ERROR);
        restError.setMessage(
                this.getRestMessage(RestErrorCodes.GLOBAL_UNEXPECTED_ERROR,
                        new Object[] { exception.getClass().getSimpleName() },
                        Locale.getDefault()));

        LOG.error("REST service exception: ", exception);
        return this.buildResponse(restError, httpRequest);
    }

    private ResponseEntity<Object> buildResponse(final RestError restError, final HttpServletRequest httpRequest) {
        if (httpRequest != null) {
            restError.setRequestUrl(getFullURL(httpRequest));
            restError.setRemoteAddress(getRemoteAddress(httpRequest));
            restError.setRequestMethod(getRequestMethod(httpRequest));
        }

        if ("PROD".equalsIgnoreCase(this.b3ApplicationContext.getDevelopmentLevel())) {
            restError.setStackTrace("<discarded - check logs>");
        }

        return new ResponseEntity<>(restError, restError.getHttpStatus());
    }

    public static String getFullURL(final HttpServletRequest request) {
        final StringBuilder requestURL = new StringBuilder(request.getRequestURL().toString());
        final String queryString = request.getQueryString();

        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }

    public static String getRemoteAddress(final HttpServletRequest httpRequest) {
        final String value = httpRequest.getHeader("X-Forwarded-For");

        if (value != null && value.length() > 0) {
            final String[] forwarderIps = value.split(",");

            if (forwarderIps != null && forwarderIps.length > 0) {
                return forwarderIps[0];
            }
        }

        return httpRequest.getRemoteAddr();
    }

    public static RequestMethod getRequestMethod(final HttpServletRequest httpRequest) {
        try {
            return RequestMethod.valueOf(httpRequest.getMethod());
        } catch (final Exception e) {
        }

        return null;
    }

    public String getServiceMessage(final ServiceException exception, final Locale locale) {
        try {
            Object[] args = null;
            if (exception.getErrors() != null && !exception.getErrors().isEmpty()) {
                args = exception.getErrors().get(0).getArguments();
            }

            return this.messageSource.getMessage(exception.getErrorCode(), args, locale);
        } catch (final Throwable e) {
            LOG.warn("Missing error message for service exception:", e);
        }

        return this.messageSource.getMessage(RestErrorCodes.GLOBAL_UNEXPECTED_ERROR,
                new Object[] { "No error msg defined for " + exception.getErrorCode() }, Locale.getDefault());
    }

    public String getRestMessage(final RestException exception, final Locale locale) {
        return this.getRestMessage(exception.getErrorCode(), exception.getErrors().get(0).getArguments(), locale);
    }

    public String getRestMessage(String errorCode, Object[] arguments, final Locale locale) {
        try {
            return this.messageSource.getMessage(errorCode, arguments, locale);
        } catch (final Throwable e) {
            LOG.warn("Missing error message for REST exception:", e);
        }

        return this.messageSource.getMessage(RestErrorCodes.GLOBAL_UNEXPECTED_ERROR,
                new Object[] { "No error msg defined for " + errorCode }, Locale.getDefault());
    }
}
