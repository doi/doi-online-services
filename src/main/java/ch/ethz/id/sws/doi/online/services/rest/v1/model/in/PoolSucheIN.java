package ch.ethz.id.sws.doi.online.services.rest.v1.model.in;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class PoolSucheIN {

    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("doi-prefix")
    private String doiPrefix = null;

    @JsonProperty("url-prefix")
    private String urlPrefix = null;

    @JsonProperty("server-url")
    private String serverUrl = null;

    @JsonProperty("from-date-pattern")
    private String fromDatePattern = null;

    @JsonProperty("metadata-prefix")
    private String metadataPrefix = null;

    @JsonProperty("set-name")
    private String setName = null;

    @JsonProperty("xslt")
    private String xslt = null;

    @JsonProperty("default-restype-general-code")
    private Integer defaultResourceTypeGeneralCode = null;

    @JsonProperty("next-schedule-start")
    private String nextScheduleStart = null;

    @JsonProperty("next-schedule-end")
    private String nextScheduleEnd = null;

    @JsonProperty("last-import-date-start")
    private String lastImportDateStart = null;

    @JsonProperty("last-import-date-end")
    private String lastImportDateEnd = null;

    @JsonProperty("last-export-date-start")
    private String lastExportDateStart = null;

    @JsonProperty("last-export-date-end")
    private String lastExportDateEnd = null;

    @JsonProperty("cron-schedule")
    private String harvestCronSchedule = null;

    @JsonProperty("cron-disabled")
    private Integer harvestCronDisabled = null;

    @JsonProperty("import-type")
    private Integer importTypeCode = null;

    @JsonProperty("batch-status")
    private Integer batchStatusCode = null;

    @JsonProperty("datacite-username")
    private String dataCiteUsername = null;

    @JsonProperty("datacite-password")
    private String dataCitePassword = null;

    @JsonProperty("batch-order")
    private String manualBatchOrder = null;

    @JsonProperty("batch-param")
    private String manualBatchParam = null;

    @JsonProperty("batch-owner")
    private String batchOrderOwner = null;

    @JsonProperty("creation-date-start")
    private String creationDateStart = null;

    @JsonProperty("creation-date-end")
    private String creationDateEnd = null;

    @JsonProperty("modification-date-start")
    private String modificationDateStart = null;

    @JsonProperty("modification-date-end")
    private String modificationDateEnd = null;

    @JsonProperty("modifying-user")
    private String modifyingUser = null;

    @JsonProperty("rs-first")
    private Long rsFirst = null;

    @JsonProperty("rs-size")
    private Long rsSize = null;

    @JsonProperty("result-sortorder-array")
    private List<ResultSortOrder> resultSortOrderList = new ArrayList<ResultSortOrder>();

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDoiPrefix() {
        return this.doiPrefix;
    }

    public void setDoiPrefix(String doiPrefix) {
        this.doiPrefix = doiPrefix;
    }

    public String getUrlPrefix() {
        return this.urlPrefix;
    }

    public void setUrlPrefix(String urlPrefix) {
        this.urlPrefix = urlPrefix;
    }

    public String getServerUrl() {
        return this.serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getFromDatePattern() {
        return this.fromDatePattern;
    }

    public void setFromDatePattern(String fromDatePattern) {
        this.fromDatePattern = fromDatePattern;
    }

    public String getMetadataPrefix() {
        return this.metadataPrefix;
    }

    public void setMetadataPrefix(String metadataPrefix) {
        this.metadataPrefix = metadataPrefix;
    }

    public String getSetName() {
        return this.setName;
    }

    public void setSetName(String setName) {
        this.setName = setName;
    }

    public String getXslt() {
        return this.xslt;
    }

    public void setXslt(String xslt) {
        this.xslt = xslt;
    }

    public Integer getDefaultResourceTypeGeneralCode() {
        return this.defaultResourceTypeGeneralCode;
    }

    public void setDefaultResourceTypeGeneralCode(Integer defaultResourceTypeGeneralCode) {
        this.defaultResourceTypeGeneralCode = defaultResourceTypeGeneralCode;
    }

    public String getHarvestCronSchedule() {
        return this.harvestCronSchedule;
    }

    public void setHarvestCronSchedule(String harvestCronSchedule) {
        this.harvestCronSchedule = harvestCronSchedule;
    }

    public Integer getHarvestCronDisabled() {
        return this.harvestCronDisabled;
    }

    public void setHarvestCronDisabled(Integer harvestCronDisabled) {
        this.harvestCronDisabled = harvestCronDisabled;
    }

    public String getDataCiteUsername() {
        return this.dataCiteUsername;
    }

    public void setDataCiteUsername(String dataCiteUsername) {
        this.dataCiteUsername = dataCiteUsername;
    }

    public String getDataCitePassword() {
        return this.dataCitePassword;
    }

    public void setDataCitePassword(String dataCitePassword) {
        this.dataCitePassword = dataCitePassword;
    }

    public String getManualBatchOrder() {
        return this.manualBatchOrder;
    }

    public void setManualBatchOrder(String manualBatchOrder) {
        this.manualBatchOrder = manualBatchOrder;
    }

    public String getBatchOrderOwner() {
        return this.batchOrderOwner;
    }

    public void setBatchOrderOwner(String batchOrderOwner) {
        this.batchOrderOwner = batchOrderOwner;
    }

    public String getCreationDateStart() {
        return this.creationDateStart;
    }

    public void setCreationDateStart(String creationDateStart) {
        this.creationDateStart = creationDateStart;
    }

    public String getCreationDateEnd() {
        return this.creationDateEnd;
    }

    public void setCreationDateEnd(String creationDateEnd) {
        this.creationDateEnd = creationDateEnd;
    }

    public String getModificationDateStart() {
        return this.modificationDateStart;
    }

    public void setModificationDateStart(String modificationDateStart) {
        this.modificationDateStart = modificationDateStart;
    }

    public String getModificationDateEnd() {
        return this.modificationDateEnd;
    }

    public void setModificationDateEnd(String modificationDateEnd) {
        this.modificationDateEnd = modificationDateEnd;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public void setModifyingUser(String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }

    public Long getRsFirst() {
        return this.rsFirst;
    }

    public void setRsFirst(Long rsFirst) {
        this.rsFirst = rsFirst;
    }

    public Long getRsSize() {
        return this.rsSize;
    }

    public void setRsSize(Long rsSize) {
        this.rsSize = rsSize;
    }

    public List<ResultSortOrder> getResultSortOrderList() {
        return this.resultSortOrderList;
    }

    public void setResultSortOrderList(List<ResultSortOrder> resultSortOrderList) {
        this.resultSortOrderList = resultSortOrderList;
    }

    public String getLastImportDateStart() {
        return this.lastImportDateStart;
    }

    public void setLastImportDateStart(String lastImportDateStart) {
        this.lastImportDateStart = lastImportDateStart;
    }

    public String getLastImportDateEnd() {
        return this.lastImportDateEnd;
    }

    public void setLastImportDateEnd(String lastImportDateEnd) {
        this.lastImportDateEnd = lastImportDateEnd;
    }

    public String getLastExportDateStart() {
        return this.lastExportDateStart;
    }

    public void setLastExportDateStart(String lastExportDateStart) {
        this.lastExportDateStart = lastExportDateStart;
    }

    public String getLastExportDateEnd() {
        return this.lastExportDateEnd;
    }

    public void setLastExportDateEnd(String lastExportDateEnd) {
        this.lastExportDateEnd = lastExportDateEnd;
    }

    public String getNextScheduleStart() {
        return this.nextScheduleStart;
    }

    public void setNextScheduleStart(String nextScheduleStart) {
        this.nextScheduleStart = nextScheduleStart;
    }

    public String getNextScheduleEnd() {
        return this.nextScheduleEnd;
    }

    public void setNextScheduleEnd(String nextScheduleEnd) {
        this.nextScheduleEnd = nextScheduleEnd;
    }

    public Integer getImportTypeCode() {
        return this.importTypeCode;
    }

    public void setImportTypeCode(Integer importTypeCode) {
        this.importTypeCode = importTypeCode;
    }

    public Integer getBatchStatusCode() {
        return this.batchStatusCode;
    }

    public void setBatchStatusCode(Integer batchStatusCode) {
        this.batchStatusCode = batchStatusCode;
    }

    public String getManualBatchParam() {
        return this.manualBatchParam;
    }

    public void setManualBatchParam(String manualBatchParam) {
        this.manualBatchParam = manualBatchParam;
    }

}
