package ch.ethz.id.sws.doi.online.services.commons;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.DashboardOUT;

public class MySseEmitter extends SseEmitter {

    private List<Long> doiPoolIdList = null;

    public MySseEmitter(List<DashboardOUT> dashboardList) {
        super(86400000L);

        this.doiPoolIdList = this.getPoolIdList(dashboardList);
    }

    private List<Long> getPoolIdList(List<DashboardOUT> dashboardList) {
        List<Long> pooldIdList = new ArrayList<Long>();

        for (DashboardOUT dashboardOUT : dashboardList) {
            pooldIdList.add(dashboardOUT.getId());
        }

        return pooldIdList;
    }

    public List<DashboardOUT> filter(List<DashboardOUT> dashboardList) {
        return dashboardList.stream().filter(dashboard -> {
            return this.doiPoolIdList.stream().anyMatch(doiPoolId -> {
                return doiPoolId.equals(dashboard.getId());
            });
        }).collect(Collectors.toList());
    }
}
