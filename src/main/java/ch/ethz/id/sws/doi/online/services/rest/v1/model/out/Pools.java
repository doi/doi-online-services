package ch.ethz.id.sws.doi.online.services.rest.v1.model.out;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Pools {

    private Long totalResultCount = null;
    private List<PoolOUT> doiPoolList = null;

    public Pools(List<PoolOUT> poolList) {
        this.doiPoolList = poolList;
    }

    @JsonProperty("total")
    public Long getTotalResultCount() {
        return this.totalResultCount;
    }

    public void setTotalResultCount(Long totalResultCount) {
        this.totalResultCount = totalResultCount;
    }

    @JsonProperty("pool-array")
    public List<PoolOUT> getDoiPoolList() {
        return this.doiPoolList;
    }

    public void setDoiPoolList(List<PoolOUT> doiPoolList) {
        this.doiPoolList = doiPoolList;
    }

}
