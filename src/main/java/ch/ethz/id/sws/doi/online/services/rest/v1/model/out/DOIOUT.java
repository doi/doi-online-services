package ch.ethz.id.sws.doi.online.services.rest.v1.model.out;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class DOIOUT {

    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("doi")
    private String doi = null;

    @JsonProperty("url")
    private String url = null;

    @JsonProperty("pool-id")
    private Long doiPoolId = null;

    @JsonProperty("pool-name")
    private String doiPoolName = null;

    @JsonProperty("metadata-json")
    private String metadataJson = null;

    @JsonProperty("import-date")
    private String importDate = null;

    @JsonProperty("export-date")
    private String exportDate = null;

    @JsonProperty("creation-date")
    private String creationDate = null;

    @JsonProperty("modification-date")
    private String modificationDate = null;

    @JsonProperty("modifying-user")
    private String modifyingUser = null;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDoi() {
        return this.doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getDoiPoolId() {
        return this.doiPoolId;
    }

    public void setDoiPoolId(Long doiPoolId) {
        this.doiPoolId = doiPoolId;
    }

    public String getMetadataJson() {
        return this.metadataJson;
    }

    public void setMetadataJson(String metadataJson) {
        this.metadataJson = metadataJson;
    }

    public String getImportDate() {
        return this.importDate;
    }

    public void setImportDate(String importDate) {
        this.importDate = importDate;
    }

    public String getExportDate() {
        return this.exportDate;
    }

    public void setExportDate(String exportDate) {
        this.exportDate = exportDate;
    }

    public String getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getModificationDate() {
        return this.modificationDate;
    }

    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public void setModifyingUser(String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }

    public String getDoiPoolName() {
        return this.doiPoolName;
    }

    public void setDoiPoolName(String doiPoolName) {
        this.doiPoolName = doiPoolName;
    }

}
