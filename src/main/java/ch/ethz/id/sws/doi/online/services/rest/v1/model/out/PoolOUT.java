package ch.ethz.id.sws.doi.online.services.rest.v1.model.out;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class PoolOUT {

    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("doi-prefix")
    private String doiPrefix = null;

    @JsonProperty("url-prefix")
    private String urlPrefix = null;

    @JsonProperty("server-url")
    private String serverUrl = null;

    @JsonProperty("from-date-pattern")
    private String fromDatePattern = null;

    @JsonProperty("metadata-prefix")
    private String metadataPrefix = null;

    @JsonProperty("set-name")
    private String setName = null;

    @JsonProperty("default-restype-general-code")
    private Integer defaultResourceTypeGeneralCode = null;

    @JsonProperty("xslt")
    private String xslt = null;

    @JsonProperty("cron-schedule")
    private String harvestCronSchedule = null;

    @JsonProperty("cron-disabled")
    private Integer harvestCronDisabled = null;

    @JsonProperty("import-type-code")
    private Integer importTypeCode = null;

    @JsonProperty("last-import-date")
    private String lastImportDate = null;

    @JsonProperty("last-export-date")
    private String lastExportDate = null;

    @JsonProperty("datacite-username")
    private String dataCiteUsername = null;

    @JsonProperty("datacite-password")
    private String dataCitePassword = null;

    @JsonProperty("batch-order")
    private String manualBatchOrder = null;

    @JsonProperty("batch-owner")
    private String batchOrderOwner = null;

    @JsonProperty("creation-date")
    private String creationDate = null;

    @JsonProperty("modification-date")
    private String modificationDate = null;

    @JsonProperty("modifying-user")
    private String modifyingUser = null;
    
    @JsonProperty("doi-tombstone")
    private String doiTombstone = null;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDoiPrefix() {
        return this.doiPrefix;
    }

    public void setDoiPrefix(String doiPrefix) {
        this.doiPrefix = doiPrefix;
    }

    public String getUrlPrefix() {
        return this.urlPrefix;
    }

    public void setUrlPrefix(String urlPrefix) {
        this.urlPrefix = urlPrefix;
    }

    public String getServerUrl() {
        return this.serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getFromDatePattern() {
        return this.fromDatePattern;
    }

    public void setFromDatePattern(String fromDatePattern) {
        this.fromDatePattern = fromDatePattern;
    }

    public String getMetadataPrefix() {
        return this.metadataPrefix;
    }

    public void setMetadataPrefix(String metadataPrefix) {
        this.metadataPrefix = metadataPrefix;
    }

    public String getSetName() {
        return this.setName;
    }

    public void setSetName(String setName) {
        this.setName = setName;
    }

    public Integer getDefaultResourceTypeGeneralCode() {
        return this.defaultResourceTypeGeneralCode;
    }

    public void setDefaultResourceTypeGeneralCode(Integer defaultResourceTypeGeneralCode) {
        this.defaultResourceTypeGeneralCode = defaultResourceTypeGeneralCode;
    }

    public String getXslt() {
        return this.xslt;
    }

    public void setXslt(String xslt) {
        this.xslt = xslt;
    }

    public String getHarvestCronSchedule() {
        return this.harvestCronSchedule;
    }

    public void setHarvestCronSchedule(String harvestCronSchedule) {
        this.harvestCronSchedule = harvestCronSchedule;
    }

    public Integer getHarvestCronDisabled() {
        return this.harvestCronDisabled;
    }

    public void setHarvestCronDisabled(Integer harvestCronDisabled) {
        this.harvestCronDisabled = harvestCronDisabled;
    }

    public String getLastImportDate() {
        return this.lastImportDate;
    }

    public void setLastImportDate(String lastImportDate) {
        this.lastImportDate = lastImportDate;
    }

    public String getLastExportDate() {
        return this.lastExportDate;
    }

    public void setLastExportDate(String lastExportDate) {
        this.lastExportDate = lastExportDate;
    }

    public String getDataCiteUsername() {
        return this.dataCiteUsername;
    }

    public void setDataCiteUsername(String dataCiteUsername) {
        this.dataCiteUsername = dataCiteUsername;
    }

    public String getDataCitePassword() {
        return this.dataCitePassword;
    }

    public void setDataCitePassword(String dataCitePassword) {
        this.dataCitePassword = dataCitePassword;
    }

    public String getManualBatchOrder() {
        return this.manualBatchOrder;
    }

    public void setManualBatchOrder(String manualBatchOrder) {
        this.manualBatchOrder = manualBatchOrder;
    }

    public String getBatchOrderOwner() {
        return this.batchOrderOwner;
    }

    public void setBatchOrderOwner(String batchOrderOwner) {
        this.batchOrderOwner = batchOrderOwner;
    }

    public String getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getModificationDate() {
        return this.modificationDate;
    }

    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public void setModifyingUser(String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }

    public Integer getImportTypeCode() {
        return this.importTypeCode;
    }

    public void setImportTypeCode(Integer importTypeCode) {
        this.importTypeCode = importTypeCode;
    }

	public String getDoiTombstone() {
		return doiTombstone;
	}

	public void setDoiTombstone(String doiTombstone) {
		this.doiTombstone = doiTombstone;
	}

}
