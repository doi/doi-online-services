package ch.ethz.id.sws.doi.online.services.rest.v1.model.out;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Users {

    private Long totalResultCount = null;
    private List<UserOUT> userList = null;

    public Users(List<UserOUT> userList) {
        this.userList = userList;
    }

    @JsonProperty("user-array")
    public List<UserOUT> getUserList() {
        return this.userList;
    }

    public void setUserList(List<UserOUT> userList) {
        this.userList = userList;
    }

    @JsonProperty("total")
    public Long getTotalResultCount() {
        return this.totalResultCount;
    }

    public void setTotalResultCount(Long totalResultCount) {
        this.totalResultCount = totalResultCount;
    }
}
