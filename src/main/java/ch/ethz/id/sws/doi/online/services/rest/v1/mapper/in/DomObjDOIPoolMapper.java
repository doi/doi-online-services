package ch.ethz.id.sws.doi.online.services.rest.v1.mapper.in;

import java.nio.charset.StandardCharsets;

import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.PoolIN;

@Mapper(componentModel = "spring")
public abstract class DomObjDOIPoolMapper {

    @Mappings({
            @Mapping(target = "dataCitePassword", ignore = true),
            @Mapping(target = "manualBatchOrder", ignore = true),
            @Mapping(target = "batchOrderOwner", ignore = true),
            @Mapping(target = "fromDatePattern", source = "fromDatePattern", defaultValue = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    })

    public abstract DomObjDOIPool map(PoolIN poolIN, @Context DomObjDOIPool domObjDOIPoolBefore) throws Exception;

    @AfterMapping
    protected void afterMapping(@MappingTarget DomObjDOIPool domObjDOIPool, PoolIN poolIN,
            @Context DomObjDOIPool domObjDOIPoolBefore) throws Exception {
        if (StringUtils.isNotEmpty(poolIN.getDataCitePassword())) {
            domObjDOIPool.setDataCitePassword(poolIN.getDataCitePassword().getBytes(StandardCharsets.UTF_8));
            domObjDOIPool.encryptDataCitePassword();
        } else {
            domObjDOIPool.setDataCitePassword(null);
        }

        if (domObjDOIPoolBefore != null) {
            // It's an update, we need to re-set the read-only data
            domObjDOIPool.setId(domObjDOIPoolBefore.getId());
            domObjDOIPool.setLastImportDate(domObjDOIPoolBefore.getLastImportDate());
            domObjDOIPool.setLastExportDate(domObjDOIPoolBefore.getLastExportDate());
            domObjDOIPool.setManualBatchOrder(domObjDOIPoolBefore.getManualBatchOrder());
            domObjDOIPool.setBatchOrderOwner(domObjDOIPoolBefore.getBatchOrderOwner());
        }
    }
}
