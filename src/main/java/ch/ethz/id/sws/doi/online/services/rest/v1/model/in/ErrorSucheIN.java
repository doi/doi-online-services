package ch.ethz.id.sws.doi.online.services.rest.v1.model.in;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class ErrorSucheIN {
    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("execution-id")
    private Long executionId = null;

    @JsonProperty("pool-id")
    private Long doiPoolId = null;

    @JsonProperty("pool-name")
    private String doiPoolName = null;

    @JsonProperty("pool-doi-prefix")
    private String doiPoolDoiPrefix = null;

    @JsonProperty("doi-id")
    private Long doiId = null;

    @JsonProperty("doi")
    private String doi = null;

    @JsonProperty("error-code")
    private String errorCode = null;

    @JsonProperty("error-msg")
    private String errorMsg = null;

    @JsonProperty("request")
    private String request = null;

    @JsonProperty("response")
    private String response = null;

    @JsonProperty("snipplet")
    private String snipplet = null;

    @JsonProperty("handled")
    private Integer handled = null;

    @JsonProperty("comment")
    private String comment = null;

    @JsonProperty("creation-date-start")
    private String creationDateStart = null;

    @JsonProperty("creation-date-end")
    private String creationDateEnd = null;

    @JsonProperty("modification-date-start")
    private String modificationDateStart = null;

    @JsonProperty("modification-date-end")
    private String modificationDateEnd = null;

    @JsonProperty("modifying-user")
    private String modifyingUser = null;

    @JsonProperty("rs-first")
    private Long rsFirst = null;

    @JsonProperty("rs-size")
    private Long rsSize = null;

    @JsonProperty("result-sortorder-array")
    private List<ResultSortOrder> resultSortOrderList = new ArrayList<ResultSortOrder>();

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExecutionId() {
        return this.executionId;
    }

    public void setExecutionId(Long executionId) {
        this.executionId = executionId;
    }

    public Long getDoiPoolId() {
        return this.doiPoolId;
    }

    public void setDoiPoolId(Long doiPoolId) {
        this.doiPoolId = doiPoolId;
    }

    public String getDoiPoolName() {
        return this.doiPoolName;
    }

    public void setDoiPoolName(String doiPoolName) {
        this.doiPoolName = doiPoolName;
    }

    public Long getDoiId() {
        return this.doiId;
    }

    public void setDoiId(Long doiId) {
        this.doiId = doiId;
    }

    public String getDoi() {
        return this.doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return this.errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getRequest() {
        return this.request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return this.response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getSnipplet() {
        return this.snipplet;
    }

    public void setSnipplet(String snipplet) {
        this.snipplet = snipplet;
    }

    public Integer getHandled() {
        return this.handled;
    }

    public void setHandled(Integer handled) {
        this.handled = handled;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public void setModifyingUser(String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }

    public Long getRsFirst() {
        return this.rsFirst;
    }

    public void setRsFirst(Long rsFirst) {
        this.rsFirst = rsFirst;
    }

    public Long getRsSize() {
        return this.rsSize;
    }

    public void setRsSize(Long rsSize) {
        this.rsSize = rsSize;
    }

    public List<ResultSortOrder> getResultSortOrderList() {
        return this.resultSortOrderList;
    }

    public void setResultSortOrderList(List<ResultSortOrder> resultSortOrderList) {
        this.resultSortOrderList = resultSortOrderList;
    }

    public String getCreationDateStart() {
        return this.creationDateStart;
    }

    public void setCreationDateStart(String creationDateStart) {
        this.creationDateStart = creationDateStart;
    }

    public String getCreationDateEnd() {
        return this.creationDateEnd;
    }

    public void setCreationDateEnd(String creationDateEnd) {
        this.creationDateEnd = creationDateEnd;
    }

    public String getModificationDateStart() {
        return this.modificationDateStart;
    }

    public void setModificationDateStart(String modificationDateStart) {
        this.modificationDateStart = modificationDateStart;
    }

    public String getModificationDateEnd() {
        return this.modificationDateEnd;
    }

    public void setModificationDateEnd(String modificationDateEnd) {
        this.modificationDateEnd = modificationDateEnd;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDoiPoolDoiPrefix() {
        return this.doiPoolDoiPrefix;
    }

    public void setDoiPoolDoiPrefix(String doiPoolDoiPrefix) {
        this.doiPoolDoiPrefix = doiPoolDoiPrefix;
    }

}
