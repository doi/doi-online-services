package ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out;

import java.util.List;

import org.mapstruct.Mapper;

import ch.ethz.id.sws.doi.commons.services.doierror.DomObjDOIError;
import ch.ethz.id.sws.doi.commons.services.doierror.DomObjDOIErrorResultat;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.ErrorOUT;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.Errors;

@Mapper(componentModel = "spring")
public abstract class ErrorsMapper {

    public abstract Errors map(DomObjDOIErrorResultat domObjDOIErrorResultat);

    public abstract List<ErrorOUT> map(List<DomObjDOIError> domObjDOIErrorList);

    public abstract ErrorOUT map(DomObjDOIError domObjDOIError);
}
