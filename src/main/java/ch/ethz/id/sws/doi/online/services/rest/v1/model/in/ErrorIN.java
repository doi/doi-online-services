package ch.ethz.id.sws.doi.online.services.rest.v1.model.in;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class ErrorIN {

    @JsonProperty("handled")
    private Integer handled = null;

    @JsonProperty("comment")
    private String comment = null;

    public Integer getHandled() {
        return this.handled;
    }

    public void setHandled(Integer handled) {
        this.handled = handled;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
