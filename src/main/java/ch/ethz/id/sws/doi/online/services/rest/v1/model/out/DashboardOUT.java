package ch.ethz.id.sws.doi.online.services.rest.v1.model.out;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class DashboardOUT {

    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("batch-status-code")
    private Integer batchStatusCode = null;

    @JsonProperty("batch-status-desc")
    private String batchStatus = null;

    @JsonProperty("import-type-code")
    private Integer importTypeCode = null;

    @JsonProperty("next-schedule")
    private String nextSchedule = null;

    @JsonProperty("last-export-date")
    private String lastExportDate = null;

    @JsonProperty("last-import-date")
    private String lastImportDate = null;

    @JsonProperty("last-error-count")
    private Long lastErrorCount = null;

    @JsonProperty("last-update-count")
    private Long lastUpdateCount = null;

    @JsonProperty("last-new-count")
    private Long lastNewCount = null;

    @JsonProperty("total-doi-count")
    private Long totalDoiCount = null;

    @JsonProperty("total-pending-exports")
    private Long pendingExports = null;

    @JsonProperty("has-credentials")
    private Boolean hasCredentials = null;

    @JsonProperty("has-serverconfig")
    private Boolean hasServerConfig = null;

    @JsonProperty("has-manual-batch-pending")
    private Boolean hasManualBatchPending = null;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNextSchedule() {
        return this.nextSchedule;
    }

    public void setNextSchedule(String nextSchedule) {
        this.nextSchedule = nextSchedule;
    }

    public String getLastExportDate() {
        return this.lastExportDate;
    }

    public void setLastExportDate(String lastExportDate) {
        this.lastExportDate = lastExportDate;
    }

    public Long getLastErrorCount() {
        return this.lastErrorCount;
    }

    public void setLastErrorCount(Long lastErrorCount) {
        this.lastErrorCount = lastErrorCount;
    }

    public Long getLastUpdateCount() {
        return this.lastUpdateCount;
    }

    public void setLastUpdateCount(Long lastUpdateCount) {
        this.lastUpdateCount = lastUpdateCount;
    }

    public Long getLastNewCount() {
        return this.lastNewCount;
    }

    public void setLastNewCount(Long lastNewCount) {
        this.lastNewCount = lastNewCount;
    }

    public Long getTotalDoiCount() {
        return this.totalDoiCount;
    }

    public void setTotalDoiCount(Long totalDoiCount) {
        this.totalDoiCount = totalDoiCount;
    }

    public String getLastImportDate() {
        return this.lastImportDate;
    }

    public void setLastImportDate(String lastImportDate) {
        this.lastImportDate = lastImportDate;
    }

    public Integer getBatchStatusCode() {
        return this.batchStatusCode;
    }

    public void setBatchStatusCode(Integer batchStatusCode) {
        this.batchStatusCode = batchStatusCode;
    }

    public Integer getImportTypeCode() {
        return this.importTypeCode;
    }

    public void setImportTypeCode(Integer importTypeCode) {
        this.importTypeCode = importTypeCode;
    }

    public String getBatchStatus() {
        return this.batchStatus;
    }

    public void setBatchStatus(String batchStatus) {
        this.batchStatus = batchStatus;
    }

    public Boolean getHasCredentials() {
        return this.hasCredentials;
    }

    public void setHasCredentials(Boolean hasCredentials) {
        this.hasCredentials = hasCredentials;
    }

    public Boolean getHasServerConfig() {
        return this.hasServerConfig;
    }

    public void setHasServerConfig(Boolean hasServerConfig) {
        this.hasServerConfig = hasServerConfig;
    }

    public Boolean getHasManualBatchPending() {
        return this.hasManualBatchPending;
    }

    public void setHasManualBatchPending(Boolean hasManualBatchPending) {
        this.hasManualBatchPending = hasManualBatchPending;
    }

    public Long getPendingExports() {
        return this.pendingExports;
    }

    public void setPendingExports(Long pendingExports) {
        this.pendingExports = pendingExports;
    }

}
