package ch.ethz.id.sws.doi.online.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import ch.ethz.id.sws.base.commons.SpringProfile;
import ch.ethz.id.sws.base.services.BaseServicesConfig;
import ch.ethz.id.sws.base.services.rest.v1.controller.HelloRestController;
import ch.ethz.id.sws.doi.online.services.rest.ComponentScanMarker;
import ch.ethz.id.sws.security.commons.ShibbolethAuthManager;
import ch.ethz.id.sws.security.commons.ShibbolethAuthTokenHeaderFilter;

@Configuration
@EnableWebMvc
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Import({ BaseServicesConfig.class })
@ComponentScan(basePackageClasses = { ComponentScanMarker.class })
@Order(10)
@Profile(value = SpringProfile.PROFILE_WLS)
public class RestSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private final ShibbolethAuthTokenHeaderFilter filter = null;

    @Override
    protected void configure(final HttpSecurity httpSecurity) throws Exception {
        this.filter.setAuthenticationManager(new ShibbolethAuthManager());

        httpSecurity
                .antMatcher("/services/**")
                .csrf().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
                .and()
                .addFilter(this.filter)
                .addFilterBefore(new ExceptionTranslationFilter(new Http403ForbiddenEntryPoint()),
                        this.filter.getClass())
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/services" + HelloRestController.HELLO_CONTEXT).permitAll()
                .anyRequest().authenticated();
    }
}
