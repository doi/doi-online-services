package ch.ethz.id.sws.doi.online.services.rest.v1.mapper.in;

import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.commons.services.dublincore.DublinCoreRecord;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.DOIIN;

@Mapper(componentModel = "spring")
public abstract class DomObjDOIMapper {

    public abstract DomObjDOI map(DOIIN doiIN, @Context DomObjDOIPool domObjDOIPool) throws Exception;

    @AfterMapping
    protected void afterMap(@MappingTarget DomObjDOI domObjDOI, DOIIN doiIN, @Context DomObjDOIPool domObjDOIPool)
            throws Exception {
        if (!StringUtils.isEmpty(doiIN.getDoi())) {
            domObjDOI.setDoi(doiIN.getDoi().toLowerCase());
        }

        if (!StringUtils.isEmpty(domObjDOI.getMetadataJson())) {
            DublinCoreRecord dublinCoreRecord = domObjDOI.getMetadataRecord();
            dublinCoreRecord.setURL(doiIN.getUrl(), domObjDOIPool.getUrlPrefix(), domObjDOIPool.getDoiTombstone());
            domObjDOI.setMetadataRecord(dublinCoreRecord);
        }
    }
}
