package ch.ethz.id.sws.doi.online.services.rest.v1.model.in;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResultSortOrder {
    @JsonProperty("attribute-name")
    private String attributeName = null;

    @JsonProperty("ascending-order")
    private Boolean ascending = null;

    public String getAttributeName() {
        return this.attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public Boolean getAscending() {
        return this.ascending;
    }

    public void setAscending(Boolean ascending) {
        this.ascending = ascending;
    }

}
