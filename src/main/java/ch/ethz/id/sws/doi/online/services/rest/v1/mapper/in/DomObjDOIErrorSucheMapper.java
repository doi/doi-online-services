package ch.ethz.id.sws.doi.online.services.rest.v1.mapper.in;

import java.util.List;

import org.mapstruct.Mapper;

import ch.ethz.id.sws.doi.commons.Sorting;
import ch.ethz.id.sws.doi.commons.services.doierror.DomObjDOIErrorSuche;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.ErrorSucheIN;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.ResultSortOrder;

@Mapper(componentModel = "spring")
public abstract class DomObjDOIErrorSucheMapper {

    public abstract DomObjDOIErrorSuche map(ErrorSucheIN errorSucheIN);

    public abstract List<Sorting> map(List<ResultSortOrder> resultSortOrderList);

    protected Sorting map(ResultSortOrder resultSortOrder) {
        Sorting sorting = new Sorting();

        switch (resultSortOrder.getAttributeName()) {
            case "id":
                sorting.setColumn("id");
                break;
            case "execution-id":
                sorting.setColumn("executionId");
                break;
            case "pool-id":
                sorting.setColumn("doiPoolId");
                break;
            case "pool-name":
                sorting.setColumn("doiPoolName");
                break;
            case "pool-doi-prefix":
                sorting.setColumn("doiPoolDoiPrefix");
                break;
            case "doi-id":
                sorting.setColumn("doiId");
                break;
            case "doi":
                sorting.setColumn("doi");
                break;
            case "error-code":
                sorting.setColumn("errorCode");
                break;
            case "error-msg":
                sorting.setColumn("errorMsg");
                break;
            case "request":
                sorting.setColumn("request");
                break;
            case "response":
                sorting.setColumn("response");
                break;
            case "snipplet":
                sorting.setColumn("snipplet");
                break;
            case "handled":
                sorting.setColumn("handled");
                break;
            case "comment":
                sorting.setColumn("comment");
                break;
            case "creation-date":
                sorting.setColumn("creationDate");
                break;
            case "modification-date":
                sorting.setColumn("modificationDate");
                break;
            case "modifying-user":
                sorting.setColumn("modifyingUser");
                break;
        }
        sorting.setAscending(resultSortOrder.getAscending());

        return sorting;
    }
}
