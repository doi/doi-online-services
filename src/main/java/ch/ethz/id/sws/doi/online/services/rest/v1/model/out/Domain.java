package ch.ethz.id.sws.doi.online.services.rest.v1.model.out;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Domain {
    private String domainName = null;
    private List<DomainValue> domainValueArray = new ArrayList<DomainValue>();
    
    @JsonProperty("domain-name")
    public String getDomainName() {
        return domainName;
    }
    
    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }
    
    @JsonProperty("domain-value-array")
    public List<DomainValue> getDomainValueArray() {
        return domainValueArray;
    }
    
    public void setDomainValueArray(List<DomainValue> domainValueArray) {
        this.domainValueArray = domainValueArray;
    }
}
