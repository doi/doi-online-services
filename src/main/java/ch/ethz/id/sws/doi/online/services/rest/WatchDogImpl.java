package ch.ethz.id.sws.doi.online.services.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import ch.ethz.id.sws.base.commons.watchdog.WatchDog;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolDAO;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPoolSuche;

@Component
public class WatchDogImpl implements WatchDog {

    @Autowired
    private DOIPoolDAO doiPoolDAO = null;

    @Override
    public void check() throws Throwable {
        this.checkDatabase();
    }

    private void checkDatabase() {
        DomObjDOIPoolSuche domObjDOIPoolSuche = new DomObjDOIPoolSuche();

        Assert.notNull(this.doiPoolDAO.searchDOIPoolCount(domObjDOIPoolSuche),
                "DOIPoolDAO.searchDOIPoolCount() returned empty result.");
    }
}
