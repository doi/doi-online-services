package ch.ethz.id.sws.doi.online.services.rest;

public class RestErrorCodes {
    public final static String ERR_MISSING_PARAMETER = "rest.global.9030";

    public final static String GLOBAL_UNEXPECTED_ERROR = "rest.global.9000";
    public final static String GLOBAL_PARAM_LANG_INVALID = "rest.global.9010";
    public final static String GLOBAL_ELEMENT_NOTFOUND = "rest.global.9020";
    public final static String GLOBAL_INPUT_UNPARSEABLE = "rest.global.9030";
    public final static String GLOBAL_NOT_AUTHORIZED = "rest.global.9040";
    public final static String GLOBAL_DELETE_OWNUSER = "rest.global.9050";
    public final static String GLOBAL_ELEMENT_ALREADYEXISTS = "rest.global.9060"; // 0: key name, 1: key value
    public final static String GLOBAL_DOWNGRADE_OWNUSER = "rest.global.9070";
    public final static String GLOBAL_ATTRIBUTEUPDATE_NOTAUTHORIZED = "rest.global.9080"; // 0: attr name
}
