package ch.ethz.id.sws.doi.online.services.rest.v1.model.out;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class History {

    private List<DOIOUT> historyList = null;
    private Long totalResultCount = null;

    public History(List<DOIOUT> doiList) {
        this.historyList = doiList;
    }

    @JsonProperty("history-array")
    public List<DOIOUT> getHistoryList() {
        return this.historyList;
    }

    public void setHistoryList(List<DOIOUT> historyList) {
        this.historyList = historyList;
    }

    @JsonProperty("total")
    public Long getTotalResultCount() {
        return this.totalResultCount;
    }

    public void setTotalResultCount(Long totalResultCount) {
        this.totalResultCount = totalResultCount;
    }

}
