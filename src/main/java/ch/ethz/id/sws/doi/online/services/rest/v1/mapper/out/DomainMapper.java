package ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out;

import java.util.List;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import ch.ethz.id.sws.base.commons.Sprache;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.Domain;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheEntry;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheService;

@Mapper(componentModel = "spring", imports = { DomainValueMapper.class })
public abstract class DomainMapper {

    @Autowired
    private DomainCacheService domainCacheService = null;

    @Autowired
    private DomainValueMapper domainValueMapper = null;

    public Domain map(String domainName, boolean inclInactive, @Context Sprache sprache) {
        List<DomainCacheEntry> domList = this.domainCacheService.getAllDomainEntries(domainName, inclInactive);
        Domain domains = new Domain();
        domains.setDomainName(domainName);
        domains.setDomainValueArray(this.domainValueMapper.mapList(domList, sprache));

        return domains;
    }
}
