package ch.ethz.id.sws.doi.online.services.rest.v1.model.out;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class DomainValue {
    private Object code = null;
    private String descShort = null;
    private String desc = null;
    private Boolean active = null;
    
    @JsonProperty("code")
    public Object getCode() {
        return code;
    }
    
    public void setCode(Object code) {
        this.code = code;
    }
    
    @JsonProperty("desc-short")
    public String getDescShort() {
        return descShort;
    }
    
    public void setDescShort(String descShort) {
        this.descShort = descShort;
    }
    
    @JsonProperty("desc")
    public String getDesc() {
        return desc;
    }
    
    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    @JsonProperty("active")
    public Boolean isActive() {
        return active;
    }
    
    public void setActive(Boolean active) {
        this.active = active;
    }
}
