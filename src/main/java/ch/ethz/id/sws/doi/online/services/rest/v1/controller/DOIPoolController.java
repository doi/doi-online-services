package ch.ethz.id.sws.doi.online.services.rest.v1.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.ethz.id.sws.base.services.rest.v1.model.out.RestError;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPoolResultat;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPoolSuche;
import ch.ethz.id.sws.doi.commons.services.user.DomObjUser;
import ch.ethz.id.sws.doi.commons.services.user.UserService;
import ch.ethz.id.sws.doi.online.services.RootConfig;
import ch.ethz.id.sws.doi.online.services.rest.RestCommons;
import ch.ethz.id.sws.doi.online.services.rest.RestErrorCodes;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.in.DomObjDOIPoolMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.in.DomObjDOIPoolSucheMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out.PoolsMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.PoolIN;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.PoolSucheIN;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.ResultOUT;

@CrossOrigin
@RestController
@RequestMapping(value = "/v1/pools")
public class DOIPoolController {
    private static Log LOG = LogFactory.getLog(DOIPoolController.class);

    @Autowired
    private final UserService userService = null;

    @Autowired
    private final PoolsMapper poolsMapper = null;

    @Autowired
    private final DomObjDOIPoolMapper domObjDOIPoolMapper = null;

    @Autowired
    private final DomObjDOIPoolSucheMapper domObjDOIPoolSucheMapper = null;

    @Autowired
    private final DOIPoolService doiPoolService = null;

    @ResponseBody
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('" + RootConfig.AUTH_DOI_ROLE_ADMIN + "')")
    public Object createPool(@RequestBody final PoolIN poolIN, final HttpServletResponse response) throws Exception {
        LOG.debug("REST createPool: " + poolIN);

        try {
            final DomObjDOIPool domObjDOIPool = this.domObjDOIPoolMapper.map(poolIN, null);
            this.doiPoolService.insertDOIPool(domObjDOIPool);

            response.setHeader("Location", RestCommons.getLocation(Long.toString(domObjDOIPool.getId()), "lang=de"));
            response.setStatus(HttpStatus.CREATED.value());

            return new ResultOUT(domObjDOIPool.getId());
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @GetMapping(value = "/{poolId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getPool(@PathVariable final long poolId, final Authentication authentication) throws Exception {
        LOG.debug("REST getPool: " + poolId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAuthorizedForPool(domObjUser, poolId)) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            final DomObjDOIPool domObjDOIPool = this.doiPoolService.getDOIPool(poolId);

            if (domObjDOIPool == null) {
                RestError.throwException(LOG, "Pool", new Object[] { poolId },
                        RestErrorCodes.GLOBAL_ELEMENT_NOTFOUND);
            }

            return this.poolsMapper.map(domObjDOIPool);
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PostMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object searchPool(@RequestBody final PoolSucheIN poolSucheIN, final Authentication authentication)
            throws Exception {
        LOG.debug("REST searchPool: " + poolSucheIN);

        try {
            final DomObjDOIPoolSuche domObjDOIPoolSuche = this.domObjDOIPoolSucheMapper.map(poolSucheIN);

            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAdmin(domObjUser)) {
                domObjDOIPoolSuche.setUserId(domObjUser.getId());
            }

            final DomObjDOIPoolResultat domObjDOIPoolResultat = this.doiPoolService.searchDOIPool(domObjDOIPoolSuche);

            return this.poolsMapper.map(domObjDOIPoolResultat);
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PutMapping(value = "/{poolId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object updatePool(@PathVariable final long poolId, @RequestBody final PoolIN poolIN,
            final HttpServletResponse response, final Authentication authentication) throws Exception {
        LOG.debug("REST updatePool: " + poolId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAuthorizedForPool(domObjUser, poolId)) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            final DomObjDOIPool domObjDOIPoolOld = this.doiPoolService.getDOIPool(poolId);
            final DomObjDOIPool domObjDOIPoolUpdated = this.domObjDOIPoolMapper.map(poolIN, domObjDOIPoolOld);

            if (!RestCommons.isAdmin(domObjUser)) {
                if (!domObjDOIPoolOld.getDoiPrefix().equals(domObjDOIPoolUpdated.getDoiPrefix())) {
                    RestError.throwException(LOG, null, new Object[] { "doi prefix" },
                            RestErrorCodes.GLOBAL_ATTRIBUTEUPDATE_NOTAUTHORIZED);
                }
                if (!domObjDOIPoolOld.getName().equals(domObjDOIPoolUpdated.getName())) {
                    RestError.throwException(LOG, null, new Object[] { "name" },
                            RestErrorCodes.GLOBAL_ATTRIBUTEUPDATE_NOTAUTHORIZED);
                }
            }

            this.doiPoolService.updateDOIPool(domObjDOIPoolUpdated);

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PutMapping(value = "/enable", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object enablePools(@RequestBody Long[] idArray,
            final HttpServletResponse response,
            final Authentication authentication) throws Exception {
        LOG.debug("REST enablePools: " + idArray);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            for (Long poolId : idArray) {
                if (!RestCommons.isAuthorizedForPool(domObjUser, poolId)) {
                    RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
                }
            }

            for (Long poolId : idArray) {
                DomObjDOIPool domObjDOIPool = this.doiPoolService.getDOIPool(poolId);
                domObjDOIPool.setHarvestCronDisabled(0);
                this.doiPoolService.updateDOIPool(domObjDOIPool);
            }

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PutMapping(value = "/disable", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object disablePools(@RequestBody Long[] idArray,
            final HttpServletResponse response,
            final Authentication authentication) throws Exception {
        LOG.debug("REST disablePools: " + idArray);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            for (Long poolId : idArray) {
                if (!RestCommons.isAuthorizedForPool(domObjUser, poolId)) {
                    RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
                }
            }

            for (Long poolId : idArray) {
                DomObjDOIPool domObjDOIPool = this.doiPoolService.getDOIPool(poolId);
                domObjDOIPool.setHarvestCronDisabled(1);
                this.doiPoolService.updateDOIPool(domObjDOIPool);
            }

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @DeleteMapping(value = "/{poolId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('" + RootConfig.AUTH_DOI_ROLE_ADMIN + "')")
    public Object deletePool(@PathVariable final long poolId,
            final HttpServletResponse response,
            final Authentication authentication) throws Exception {
        LOG.debug("REST deletePool: " + poolId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAuthorizedForPool(domObjUser, poolId)) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }
            this.doiPoolService.startDelete(poolId);

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('" + RootConfig.AUTH_DOI_ROLE_ADMIN + "')")
    public Object deletePools(@RequestParam(value = "ids", required = true) Long[] idArray,
            final HttpServletResponse response,
            final Authentication authentication) throws Exception {
        LOG.debug("REST deletePools: " + idArray);

        try {
            for (Long poolId : idArray) {
                this.doiPoolService.startDelete(poolId);
            }

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }
}
