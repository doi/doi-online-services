package ch.ethz.id.sws.doi.online.services.rest.v1.model;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class User {
    private String originIP = null;
    private String signOnAppId = null;
    private String matriculationNr = null;
    private String homeOrg = null;
    private List<String> affiliations = null;
    private String homeOrgType = null;
    private String email = null;
    private String givenName = null;
    private String surname = null;
    private String uniqueId = null;
    private String authDate = null;
    private String idp = null;;
    private String authMethod = null;
    private List<GrantedAuthority> grantedAuthorities = null;
    private String loginname = null;
    private Long persId = null;
    private Long nPid = null;
    private String geschlecht = null;
    private String anrede = null;
    private String language = null;
    private String birthdate = null;
    private ServerVersion serverVersion = new ServerVersion();

    public String getOriginIP() {
        return originIP;
    }

    public void setOriginIP(String originIP) {
        this.originIP = originIP;
    }

    public String getSignOnAppId() {
        return signOnAppId;
    }

    public void setSignOnAppId(String signOnAppId) {
        this.signOnAppId = signOnAppId;
    }

    public String getMatriculationNr() {
        return matriculationNr;
    }

    public void setMatriculationNr(String matriculationNr) {
        this.matriculationNr = matriculationNr;
    }

    public String getHomeOrg() {
        return homeOrg;
    }

    public void setHomeOrg(String homeOrg) {
        this.homeOrg = homeOrg;
    }

    public List<String> getAffiliations() {
        return affiliations;
    }

    public void setAffiliations(List<String> affiliations) {
        this.affiliations = affiliations;
    }

    public String getHomeOrgType() {
        return homeOrgType;
    }

    public void setHomeOrgType(String homeOrgType) {
        this.homeOrgType = homeOrgType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getAuthDate() {
        return authDate;
    }

    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    public String getIdp() {
        return idp;
    }

    public void setIdp(String idp) {
        this.idp = idp;
    }

    public String getAuthMethod() {
        return authMethod;
    }

    public void setAuthMethod(String authMethod) {
        this.authMethod = authMethod;
    }

    public List<GrantedAuthority> getGrantedAuthorities() {
        return grantedAuthorities;
    }

    public void setGrantedAuthorities(List<GrantedAuthority> grantedAuthorities) {
        this.grantedAuthorities = grantedAuthorities;
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    public String getGeschlecht() {
        return geschlecht;
    }

    public void setGeschlecht(String geschlecht) {
        this.geschlecht = geschlecht;
    }

    public String getAnrede() {
        return anrede;
    }

    public void setAnrede(String anrede) {
        this.anrede = anrede;
    }

    public Long getPersId() {
        return persId;
    }

    public void setPersId(Long persId) {
        this.persId = persId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public Long getnPid() {
        return nPid;
    }

    public void setnPid(Long nPid) {
        this.nPid = nPid;
    }

    public ServerVersion getServerVersion() {
        return serverVersion;
    }

    public void setServerVersion(ServerVersion serverVersion) {
        this.serverVersion = serverVersion;
    }

    public class ServerVersion {
        private String devStage = null;
        private String serverName = null;
        private Long systemStartupTime = null;
        private Long appStartupTime = null;

        private String implementationBuild = null;
        private String implementationVersion = null;
        private String implementationBranch = null;
        private String implementationBuildDate = null;

        public String getImplementationBuild() {
            return implementationBuild;
        }

        public void setImplementationBuild(String implementationBuild) {
            this.implementationBuild = implementationBuild;
        }

        public String getImplementationVersion() {
            return implementationVersion;
        }

        public void setImplementationVersion(String implementationVersion) {
            this.implementationVersion = implementationVersion;
        }

        public String getImplementationBranch() {
            return implementationBranch;
        }

        public void setImplementationBranch(String implementationBranch) {
            this.implementationBranch = implementationBranch;
        }

        public String getImplementationBuildDate() {
            return implementationBuildDate;
        }

        public void setImplementationBuildDate(String implementationBuildDate) {
            this.implementationBuildDate = implementationBuildDate;
        }

        public String getDevStage() {
            return devStage;
        }

        public void setDevStage(String devStage) {
            this.devStage = devStage;
        }

        public String getServerName() {
            return serverName;
        }

        public void setServerName(String serverName) {
            this.serverName = serverName;
        }

        public Long getSystemStartupTime() {
            return systemStartupTime;
        }

        public void setSystemStartupTime(Long systemStartupTime) {
            this.systemStartupTime = systemStartupTime;
        }

        public Long getAppStartupTime() {
            return appStartupTime;
        }

        public void setAppStartupTime(Long appStartupTime) {
            this.appStartupTime = appStartupTime;
        }
    }
}
