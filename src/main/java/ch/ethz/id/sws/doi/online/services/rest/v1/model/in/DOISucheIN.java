package ch.ethz.id.sws.doi.online.services.rest.v1.model.in;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class DOISucheIN {

    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("doi")
    private String doi = null;

    @JsonProperty("url")
    private String url = null;

    @JsonProperty("pool-id")
    private Long doiPoolId = null;

    @JsonProperty("pool-name")
    private String doiPoolName = null;

    @JsonProperty("metadata-json")
    private String metadataJson = null;

    @JsonProperty("import-date-start")
    private String importDateStart = null;

    @JsonProperty("import-date-end")
    private String importDateEnd = null;

    @JsonProperty("export-date-start")
    private String exportDateStart = null;

    @JsonProperty("export-date-end")
    private String exportDateEnd = null;

    @JsonProperty("creation-date-start")
    private String creationDateStart = null;

    @JsonProperty("creation-date-end")
    private String creationDateEnd = null;

    @JsonProperty("modification-date-start")
    private String modificationDateStart = null;

    @JsonProperty("modification-date-end")
    private String modificationDateEnd = null;

    @JsonProperty("modifying-user")
    private String modifyingUser = null;

    @JsonProperty("rs-first")
    private Long rsFirst = null;

    @JsonProperty("rs-size")
    private Long rsSize = null;

    @JsonProperty("result-sortorder-array")
    private List<ResultSortOrder> resultSortOrderList = new ArrayList<ResultSortOrder>();

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDoi() {
        return this.doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getDoiPoolId() {
        return this.doiPoolId;
    }

    public void setDoiPoolId(Long doiPoolId) {
        this.doiPoolId = doiPoolId;
    }

    public String getMetadataJson() {
        return this.metadataJson;
    }

    public void setMetadataJson(String metadataJson) {
        this.metadataJson = metadataJson;
    }

    public Long getRsFirst() {
        return this.rsFirst;
    }

    public void setRsFirst(Long rsFirst) {
        this.rsFirst = rsFirst;
    }

    public String getCreationDateStart() {
        return this.creationDateStart;
    }

    public void setCreationDateStart(String creationDateStart) {
        this.creationDateStart = creationDateStart;
    }

    public String getCreationDateEnd() {
        return this.creationDateEnd;
    }

    public void setCreationDateEnd(String creationDateEnd) {
        this.creationDateEnd = creationDateEnd;
    }

    public String getModificationDateStart() {
        return this.modificationDateStart;
    }

    public void setModificationDateStart(String modificationDateStart) {
        this.modificationDateStart = modificationDateStart;
    }

    public String getModificationDateEnd() {
        return this.modificationDateEnd;
    }

    public void setModificationDateEnd(String modificationDateEnd) {
        this.modificationDateEnd = modificationDateEnd;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public void setModifyingUser(String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }

    public Long getRsSize() {
        return this.rsSize;
    }

    public void setRsSize(Long rsSize) {
        this.rsSize = rsSize;
    }

    public String getDoiPoolName() {
        return this.doiPoolName;
    }

    public void setDoiPoolName(String doiPoolName) {
        this.doiPoolName = doiPoolName;
    }

    public List<ResultSortOrder> getResultSortOrderList() {
        return this.resultSortOrderList;
    }

    public void setResultSortOrderList(List<ResultSortOrder> resultSortOrderList) {
        this.resultSortOrderList = resultSortOrderList;
    }

    public String getImportDateStart() {
        return this.importDateStart;
    }

    public void setImportDateStart(String importDateStart) {
        this.importDateStart = importDateStart;
    }

    public String getImportDateEnd() {
        return this.importDateEnd;
    }

    public void setImportDateEnd(String importDateEnd) {
        this.importDateEnd = importDateEnd;
    }

    public String getExportDateStart() {
        return this.exportDateStart;
    }

    public void setExportDateStart(String exportDateStart) {
        this.exportDateStart = exportDateStart;
    }

    public String getExportDateEnd() {
        return this.exportDateEnd;
    }

    public void setExportDateEnd(String exportDateEnd) {
        this.exportDateEnd = exportDateEnd;
    }

}
