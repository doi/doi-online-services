package ch.ethz.id.sws.doi.online.services.rest.v1.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.ethz.id.sws.base.services.rest.v1.model.out.RestError;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolService;
import ch.ethz.id.sws.doi.commons.services.user.DomObjUser;
import ch.ethz.id.sws.doi.commons.services.user.UserService;
import ch.ethz.id.sws.doi.online.services.RootConfig;
import ch.ethz.id.sws.doi.online.services.rest.RestCommons;
import ch.ethz.id.sws.doi.online.services.rest.RestErrorCodes;

@CrossOrigin
@RestController
@RequestMapping(value = "/v1/pools")
public class DOIBatchController {
    private static Log LOG = LogFactory.getLog(DOIBatchController.class);

    @Autowired
    private final UserService userService = null;

    @Autowired
    private final DOIPoolService doiPoolService = null;

    @ResponseBody
    @PutMapping(value = "/{poolId}/fullimport", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('" + RootConfig.AUTH_DOI_ROLE_ADMIN + "')")
    public Object startFullImport(@PathVariable final long poolId, final HttpServletResponse response,
            final Authentication authentication) throws Exception {
        LOG.debug("REST startFullImport: " + poolId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAuthorizedForPool(domObjUser, poolId)) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            this.doiPoolService.startFullImport(poolId);

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PutMapping(value = "/{poolId}/import", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object startImport(@PathVariable final long poolId, final HttpServletResponse response,
            final Authentication authentication) throws Exception {
        LOG.debug("REST startImport: " + poolId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAuthorizedForPool(domObjUser, poolId)) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            this.doiPoolService.startImport(poolId);

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PutMapping(value = "/{poolId}/export", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object startExport(@PathVariable final long poolId, final HttpServletResponse response,
            final Authentication authentication) throws Exception {
        LOG.debug("REST startExport: " + poolId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAuthorizedForPool(domObjUser, poolId)) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            this.doiPoolService.startExport(poolId);

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PutMapping(value = "/{poolId}/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object startUpdate(@PathVariable final long poolId, final HttpServletResponse response,
            final Authentication authentication) throws Exception {
        LOG.debug("REST startUpdate: " + poolId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAuthorizedForPool(domObjUser, poolId)) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            this.doiPoolService.startUpdate(poolId);

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PutMapping(value = "/{poolId}/fullexport", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('" + RootConfig.AUTH_DOI_ROLE_ADMIN + "')")
    public Object startFullExport(@PathVariable final long poolId, final HttpServletResponse response,
            final Authentication authentication) throws Exception {
        LOG.debug("REST startFullExport: " + poolId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAuthorizedForPool(domObjUser, poolId)) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            this.doiPoolService.startFullExport(poolId);

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PutMapping(value = "/{poolId}/clear", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('" + RootConfig.AUTH_DOI_ROLE_ADMIN + "')")
    public Object startClear(@PathVariable final long poolId, final HttpServletResponse response,
            final Authentication authentication) throws Exception {
        LOG.debug("REST startClear: " + poolId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAuthorizedForPool(domObjUser, poolId)) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            this.doiPoolService.startClear(poolId);

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }
}
