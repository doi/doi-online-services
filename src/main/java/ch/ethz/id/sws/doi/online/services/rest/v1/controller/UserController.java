package ch.ethz.id.sws.doi.online.services.rest.v1.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.ethz.id.sws.base.services.rest.v1.model.out.RestError;
import ch.ethz.id.sws.doi.commons.services.user.DomObjUser;
import ch.ethz.id.sws.doi.commons.services.user.DomObjUserResultat;
import ch.ethz.id.sws.doi.commons.services.user.DomObjUserSuche;
import ch.ethz.id.sws.doi.commons.services.user.UserService;
import ch.ethz.id.sws.doi.online.services.RootConfig;
import ch.ethz.id.sws.doi.online.services.rest.RestCommons;
import ch.ethz.id.sws.doi.online.services.rest.RestErrorCodes;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.in.DomObjUserMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.in.DomObjUserSucheMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out.UsersMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.UserIN;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.UserSucheIN;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.ResultOUT;

@CrossOrigin
@RestController
@RequestMapping(value = "/v1/users")
public class UserController {
    private static Log LOG = LogFactory.getLog(UserController.class);

    @Autowired
    private final UserService userService = null;

    @Autowired
    private final UsersMapper usersMapper = null;

    @Autowired
    private final DomObjUserMapper domObjUserMapper = null;

    @Autowired
    private final DomObjUserSucheMapper domObjUserSucheMapper = null;

    @ResponseBody
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('" + RootConfig.AUTH_DOI_ROLE_ADMIN + "')")
    public Object createUser(@RequestBody final UserIN userIN, final HttpServletResponse response) throws Exception {
        LOG.debug("REST createUser: " + userIN);

        try {
            final DomObjUser domObjUser = this.domObjUserMapper.map(userIN);
            this.userService.insertUser(domObjUser, userIN.getDoiPoolList());

            response.setHeader("Location", RestCommons.getLocation(Long.toString(domObjUser.getId()), "lang=de"));
            response.setStatus(HttpStatus.CREATED.value());

            return new ResultOUT(domObjUser.getId());
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @GetMapping(value = "/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getUser(@PathVariable final long userId, final Authentication authentication) throws Exception {
        LOG.debug("REST getUser: " + userId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser actingUser = this.userService.getUserByUniqueId(uniqueId);
            if (actingUser.getId() != userId && !RestCommons.isAdmin(actingUser)) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            final DomObjUser domObjUser = this.userService.getUser(userId);

            if (domObjUser == null) {
                RestError.throwException(LOG, "User", new Object[] { userId },
                        RestErrorCodes.GLOBAL_ELEMENT_NOTFOUND);
            }

            return this.usersMapper.map(domObjUser);
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PostMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('" + RootConfig.AUTH_DOI_ROLE_ADMIN + "')")
    public Object searchUser(@RequestBody final UserSucheIN userSucheIN) throws Exception {
        LOG.debug("REST searchUser: " + userSucheIN);

        try {
            final DomObjUserSuche domObjUserSuche = this.domObjUserSucheMapper.map(userSucheIN);
            final DomObjUserResultat domObjUserResultat = this.userService.searchUser(domObjUserSuche);

            return this.usersMapper.map(domObjUserResultat);
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PutMapping(value = "/{userId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('" + RootConfig.AUTH_DOI_ROLE_ADMIN + "')")
    public Object updateUser(@PathVariable final long userId, @RequestBody final UserIN userIN,
            final Authentication authentication,
            final HttpServletResponse response) throws Exception {
        LOG.debug("REST updateUser: " + userIN);

        try {
            final DomObjUser domObjUserNew = this.domObjUserMapper.map(userIN);
            domObjUserNew.setId(userId);

            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUserLocal = this.userService.getUserByUniqueId(uniqueId);
            if (domObjUserLocal.getId().equals(userId)) {
                if (domObjUserNew.getAdmin() == 0) {
                    RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_DOWNGRADE_OWNUSER);
                }
            }

            this.userService.updateUser(domObjUserNew);
            this.userService.setPools(userId, userIN.getDoiPoolList());

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @DeleteMapping(value = "/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('" + RootConfig.AUTH_DOI_ROLE_ADMIN + "')")
    public Object deleteUser(@PathVariable final long userId,
            final HttpServletResponse response,
            final Authentication authentication) throws Exception {
        LOG.debug("REST deleteUser: " + userId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (domObjUser.getId().equals(userId)) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_DELETE_OWNUSER);
            }

            this.userService.deleteUser(userId);

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('" + RootConfig.AUTH_DOI_ROLE_ADMIN + "')")
    public Object deleteUsers(@RequestParam(value = "ids", required = true) Long[] idArray,
            final HttpServletResponse response,
            final Authentication authentication) throws Exception {
        LOG.debug("REST deleteUsers: " + idArray);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);

            for (Long id : idArray) {
                if (domObjUser.getId().equals(id)) {
                    RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_DELETE_OWNUSER);
                }
            }

            for (Long id : idArray) {
                this.userService.deleteUser(id);
            }

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @PostMapping(value = "/{userId}/pools/{poolId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('" + RootConfig.AUTH_DOI_ROLE_ADMIN + "')")
    public Object addUserToPool(final String lang, @PathVariable final long userId, @PathVariable final long poolId,
            final HttpServletResponse response, final Authentication authentication) throws Exception {
        LOG.debug("REST addUserToPool: userId = " + userId + ", poolId = " + poolId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAuthorizedForPool(domObjUser, poolId)) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            this.userService.addUserToPool(userId, poolId);

            response.setStatus(HttpStatus.CREATED.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @DeleteMapping(value = "/{userId}/pools/{poolId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('" + RootConfig.AUTH_DOI_ROLE_ADMIN + "')")
    public Object removeUserToPool(final String lang, @PathVariable final long userId, @PathVariable final long poolId,
            final HttpServletResponse response, final Authentication authentication) throws Exception {
        LOG.debug("REST removeUserToPool: userId = " + userId + ", poolId = " + poolId);

        try {
            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAuthorizedForPool(domObjUser, poolId)) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            this.userService.removeUserFromPool(userId, poolId);

            response.setStatus(HttpStatus.NO_CONTENT.value());
            return "";
        } catch (final Throwable t) {
            throw t;
        }
    }
}
