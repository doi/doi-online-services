package ch.ethz.id.sws.doi.online.services.rest.v1.mapper.in;

import java.util.List;

import org.mapstruct.Mapper;

import ch.ethz.id.sws.doi.commons.Sorting;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPoolSuche;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.PoolSucheIN;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.ResultSortOrder;

@Mapper(componentModel = "spring")
public abstract class DomObjDOIPoolSucheMapper {

    public abstract DomObjDOIPoolSuche map(PoolSucheIN poolSucheIN);

    public abstract List<Sorting> map(List<ResultSortOrder> resultSortOrderList);

    protected Sorting map(ResultSortOrder resultSortOrder) {
        Sorting sorting = new Sorting();

        switch (resultSortOrder.getAttributeName()) {
            case "id":
                sorting.setColumn("id");
                break;
            case "name":
                sorting.setColumn("name");
                break;
            case "doi-prefix":
                sorting.setColumn("doiPrefix");
                break;
            case "url-prefix":
                sorting.setColumn("urlPrefix");
                break;
            case "server-url":
                sorting.setColumn("serverUrl");
                break;
            case "from-date-pattern":
                sorting.setColumn("fromDatePattern");
                break;
            case "metadata-prefix":
                sorting.setColumn("metadataPrefix");
                break;
            case "set-name":
                sorting.setColumn("setName");
                break;
            case "xslt":
                sorting.setColumn("xslt");
                break;
            case "default-restype-general-code":
                sorting.setColumn("defaultResourceTypeGeneralCode");
                break;
            case "import-type-code":
                sorting.setColumn("importType");
                break;
            case "batch-status-code":
                sorting.setColumn("batchStatus");
                break;
            case "next-schedule":
                sorting.setColumn("nextSchedule");
                break;
            case "last-import-date":
                sorting.setColumn("lastImportDate");
                break;
            case "last-export-date":
                sorting.setColumn("lastExportDate");
                break;
            case "cron-schedule":
                sorting.setColumn("harvestCronSchedule");
                break;
            case "cron-disabled":
                sorting.setColumn("harvestCronDisabled");
                break;
            case "datacite-username":
                sorting.setColumn("dataCiteUsername");
                break;
            case "datacite-password":
                sorting.setColumn("dataCitePassword");
                break;
            case "batch-order":
                sorting.setColumn("manualBatchOrder");
                break;
            case "batch-param":
                sorting.setColumn("manualBatchParam");
                break;
            case "batch-owner":
                sorting.setColumn("batchOrderOwner");
                break;
            case "creation-date":
                sorting.setColumn("creationDate");
                break;
            case "modification-date":
                sorting.setColumn("modificationDate");
                break;
            case "modifying-user":
                sorting.setColumn("modifyingUser");
                break;
        }
        sorting.setAscending(resultSortOrder.getAscending());

        return sorting;
    }
}
