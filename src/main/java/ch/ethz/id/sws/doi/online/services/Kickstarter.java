package ch.ethz.id.sws.doi.online.services;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.web.WebApplicationInitializer;

import ch.ethz.id.sws.base.services.AbstractKickstarter;

public class Kickstarter extends AbstractKickstarter implements WebApplicationInitializer {

    @Override
    public void onStartup(final ServletContext servletContext) throws ServletException {
        super.main(servletContext);
    }

    @Override
    public Class getRootConfig() {
        return RootConfig.class;
    }

    @Override
    public Class getRESTConfig() {
        return RestSecurityConfig.class;
    }

    @Override
    public Class getWebConfig() {
        return WebSecurityConfig.class;
    }

    @Override
    public boolean hasSecurityEnabled() {
        return true;
    }
}
