package ch.ethz.id.sws.doi.online.services.rest.v1.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.ethz.id.sws.base.services.rest.v1.model.out.RestError;
import ch.ethz.id.sws.doi.commons.services.doi.DOIService;
import ch.ethz.id.sws.doi.commons.services.doi.DomObjDOI;
import ch.ethz.id.sws.doi.commons.services.doihistory.DomObjDOIHistory;
import ch.ethz.id.sws.doi.commons.services.doihistory.DomObjDOIHistoryResultat;
import ch.ethz.id.sws.doi.commons.services.doihistory.DomObjDOIHistorySuche;
import ch.ethz.id.sws.doi.commons.services.user.DomObjUser;
import ch.ethz.id.sws.doi.commons.services.user.UserService;
import ch.ethz.id.sws.doi.online.services.rest.RestCommons;
import ch.ethz.id.sws.doi.online.services.rest.RestErrorCodes;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.in.DomObjDOIHistorySucheMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.mapper.out.HistoryMapper;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.in.HistorySucheIN;
import ch.ethz.id.sws.doi.online.services.rest.v1.model.out.DOIOUT;

@CrossOrigin
@RestController
@RequestMapping(value = "/v1/dois")
public class DOIHistoryController {

    private static Log LOG = LogFactory.getLog(DOIHistoryController.class);

    @Autowired
    private final UserService userService = null;

    @Autowired
    private final DOIService doiService = null;

    @Autowired
    private final HistoryMapper historyMapper = null;

    @Autowired
    private final DomObjDOIHistorySucheMapper domObjDOIHistorySucheMapper = null;

    @ResponseBody
    @GetMapping(value = "/history", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object searchHistory(@RequestBody final HistorySucheIN historySucheIN, final Authentication authentication)
            throws Exception {
        LOG.debug("REST searchHistory: " + historySucheIN);

        try {
            final DomObjDOIHistorySuche domObjDOIHistorySuche = this.domObjDOIHistorySucheMapper.map(historySucheIN);

            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAdmin(domObjUser)) {
                domObjDOIHistorySuche.setUserId(domObjUser.getId());
            }

            final DomObjDOIHistoryResultat domObjDOIHistoryResultat = this.doiService
                    .searchDOIHistory(domObjDOIHistorySuche);

            return this.historyMapper.map(domObjDOIHistoryResultat);
        } catch (final Throwable t) {
            throw t;
        }
    }

    @ResponseBody
    @GetMapping(value = "/{doiId}/history", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getHistory(@PathVariable final long doiId, final Authentication authentication) throws Exception {
        LOG.debug("REST getHistory: " + doiId);

        try {
            final DomObjDOI domObjDOI = this.doiService.getDOI(doiId);

            if (domObjDOI == null) {
                RestError.throwException(LOG, "DOI", new Object[] { doiId },
                        RestErrorCodes.GLOBAL_ELEMENT_NOTFOUND);
            }

            String uniqueId = RestCommons.getUniqueId(authentication);
            DomObjUser domObjUser = this.userService.getUserByUniqueId(uniqueId);
            if (!RestCommons.isAuthorizedForPool(domObjUser, domObjDOI.getDoiPoolId())) {
                RestError.throwException(LOG, null, new Object[] {}, RestErrorCodes.GLOBAL_NOT_AUTHORIZED);
            }

            final List<DomObjDOIHistory> domObjDOIHistoryList = this.doiService.getHistoryByDOIId(doiId);

            final DOIOUT doiOUT = this.historyMapper.map(domObjDOI);
            List<DOIOUT> doiOUTList = this.historyMapper.map(domObjDOIHistoryList);
            doiOUTList.add(0, doiOUT);

            return doiOUTList;
        } catch (final Throwable t) {
            throw t;
        }
    }
}
